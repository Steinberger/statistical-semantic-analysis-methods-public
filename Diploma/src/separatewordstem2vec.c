//  Copyright 2013 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>

#define MAX_STRING 100                                                  // Maximum length of string
#define EXP_TABLE_SIZE 1000                                             // Maximum size of EXP table
#define MAX_EXP 6                                                       // Maximum parameter of exp!
#define MAX_SENTENCE_LENGTH 1000                                        // Longer sentences are ignored
#define MAX_CODE_LENGTH 40                                              // Max code length of Huffman code

#define STEM_DELIM "|"                                                  // Stemming delimiter from preprocessed training corpus

const int vocab_hash_size = 30000000;                                   // Maximum 30 * 0.7 = 21M words in the vocabulary, apply also for ngrams

// Just rename float to real
typedef float real;                                                     // Precision of float numbers

/**
 Structure representing suffix in vocabulary.
 */
struct vocab_suf {                                                       // Single suffix
    long long cn;                                                        // Count - suffix occurrences
    char *suf;                                                           // String representation
};

/**
 * Structure representing stem in vocabulary.
 */
struct vocab_stem {                                                      // Single stem
    long long cn;                                                        // Count - stem occurrences
    char* stem;                                                          // String representation
};

/**
 * Structure representing word in vocabulary
 */
struct vocab_word {
    long long cn;                                                       // Count - word occurrences
    int *point, suf, stem;                                              // Sequence of decision nodes (parent indices) in decision tree, suffix index, stem index
    char *word, *code, codelen;                                         // String representation, number of grams, Huffman code, Length of huffman code
};

char train_file[MAX_STRING], output_file[MAX_STRING];                   // Train/output file paths
char save_vocab_file[MAX_STRING], read_vocab_file[MAX_STRING];          // Save/Load vocabulary paths
struct vocab_suf *vocabs;                                               // Vocabulary of suffixes
struct vocab_word *vocabw;                                              // Vocabulary of words
struct vocab_stem *vocabm;                                              // Vocabulary of stems
int binary = 0;                                                         // Binary representation of results or not
int cbow = 0;                                                           // CBOW or Skip-gram
int debug_mode = 2;                                                     // Level of debug
int window = 5;                                                         // Maximum! one-side! length of window -> randomly choosing
int min_count = 5;                                                      // Min word occurrences threshold
int num_threads = 1;                                                    // Number of parallel threads
int unknown = 1;                                                        // Unknown n-gram switch -> default on
int min_reduce = 1;                                                     // Another min word occurrences threshold -> additional reducing
int *vocabs_hash;                                                       // Vocabs_hash[hash(suffix)] = index -> vocabs[index] = suffix
int *vocabw_hash;                                                       // Vocabw_hash[hash(word)] = index -> vocabw[index] = word
int *vocabm_hash;                                                       // Vocabm_hash[hash(stem)] = index -> vocabm[index] = stem
long long vocabs_max_size = 1000;                                       // Current max size of suffix vocabulary -> dynamically expanding
long long vocabw_max_size = 1000;                                       // Current max size of word vocabulary -> dynamically expanding
long long vocabm_max_size = 1000;                                       // Current max size of stem vocabulary -> dynamically expanding
long long vocabs_size = 0;                                              // Actual size of suffix vocabulary
long long vocabw_size = 0;                                              // Actual size of word vocabulary
long long vocabm_size = 0;                                              // Acutal size of stem vocabulary
long long layer1_size = 100;                                            // Size (dimension) of all layers in neural network
long long iter = 5;                                                     // Number of iterations
long long train_words = 0;                                              // Number of train words (all words -> including multiple occurrences of same word)
long long word_count_actual = 0;                                        // Already trained words
long long file_size = 0;                                                // Size of input file
long long classes = 0;                                                  // Number of classification classes for K-means
real alpha = 0.025, starting_alpha;                                     // Current and starting alpha -> learning rate
real sample = 1e-3;                                                     // Subsampling value...If sample > 0 -> subsampling on!
real *syn0;                                                             // Input -> Projection weights  - Vocab * layer_size
real *syn1;                                                             // HS: Projection -> Output weights - Vocab * layer_size
real *syn1neg;                                                          // NEG: Projection -> Output weights - Vocab * layer_size
real *expTable;
clock_t start;                                                          // Start time for monitoring

int hs = 0;                                                             // Hiearchical softmax
int negative = 5;                                                       // Negative sampling value -> negative > 0 = sampling on
const int table_size = 1e8;                                             // Distribution table for Negative sampling
int *table;                                                             // Distribution table itself

char *UNKNOWN = "UNKNOWN";                                              // Unknown stem or suffix
char *NOSUF = "NOSUF";                                                  // No suffix

/**
 * Print parsed mapping of words to stem + suffix.
 */
void printWord2stemsuf() {
    int w;
    printf("-------------------\n");
    printf("Printing word2grams\n");
    printf("-------------------\n");
    for(w = 1; w < vocabw_size; w++) {
        if(vocabw[w].suf != -1) printf("%s -> %s | %s\n", vocabw[w].word, vocabm[vocabw[w].stem].stem, vocabs[vocabw[w].suf].suf);
        else printf("%s -> %s\n", vocabw[w].word, vocabm[vocabw[w].stem].stem);
    }
    printf("-------------------\n");
    printf("End of word2grams\n");
    printf("-------------------\n");
}

/**
 Create unigram table from words

 Only if Negative sampling!!! Created from sorted table.
 */
void InitUnigramTable() {
    int a, i;
    double train_words_pow = 0;                                                       // Summary of powered values
    double d1, power = 0.75;                                                          // Power constant
    table = (int *)malloc(table_size * sizeof(int));                                  // Allocate table
    for (a = 0; a < vocabw_size; a++) train_words_pow += pow(vocabw[a].cn, power);    // Some summary of powers of counts
    i = 0;                                                                            // Start at the beginning!
    d1 = pow(vocabw[i].cn, power) / train_words_pow;                                  // How many percent of all words
    for (a = 0; a < table_size; a++) {                                                // Loop over the table
        table[a] = i;                                                                 // Current word
        if (a / (double)table_size > d1) {                                            // If percentage bigger -> go for next word
            i++;                                                                      // Next word
            d1 += pow(vocabw[i].cn, power) / train_words_pow;                         // Next percentage
        }
        if (i >= vocabw_size) i = vocabw_size - 1;                                    // No more words -> use last word
    }
}

/**
 Read word. End of line -> </s>

 Native comment:
 Reads a single word from a file, assuming space + tab + EOL to be word boundaries
 */
void ReadWord(char *word, FILE *fin) {
    int a = 0, ch;
    while (!feof(fin)) {
        ch = fgetc(fin);                                            // Read char
        if (ch == 13) continue;                                     // CR - continue
        if ((ch == ' ') || (ch == '\t') || (ch == '\n')) {
            if (a > 0) {
                if (ch == '\n') ungetc(ch, fin);                    // Return back \n
                break;                                              // Stop adding
            }
            if (ch == '\n') {                                       // End of line
                strcpy(word, (char *)"</s>");                       // End of line = </s>
                return;
            } else continue;                                        // Strip whitespaces
        }
        word[a] = ch;                                               // Add next character
        a++;
        if (a >= MAX_STRING - 1) a--;                               // Truncate too long words
    }
    word[a] = 0;                                                    // Add end of string \0
}

/**
 Word->hashCode()


 Native comment:
 Returns hash value of a word
 */
int StringHash(char *string) {
    unsigned long long a, hash = 0;
    for (a = 0; a < strlen(string); a++) hash = hash * 257 + string[a];       // Hash function
    hash = hash % vocab_hash_size;                                            // Fit vocab size
    return hash;                                                              // Return hash
}

/**
 Index of suffix in vocabulary.

 Native comment:
 Returns position of a suffix in the vocabulary; if the suffix is not found, returns -1
 */
int SearchVocabs(char *suf) {
    unsigned int hash = StringHash(suf);                                // Get hash code
    while (1) {
        if (vocabs_hash[hash] == -1) return -1;                          // Suffix not found -> -1
        if (!strcmp(suf, vocabs[vocabs_hash[hash]].suf)) return vocabs_hash[hash];
        hash = (hash + 1) % vocab_hash_size;                             // Hash collision -> hash + 1
    }
    return -1;                                                           // Suffix not found -> -1
}

/**
 Index of word in vocabulary.
 Returns position of a word in the vocabulary; if the word is not found, returns -1
 */
int SearchVocabw(char *word) {
    unsigned int hash = StringHash(word);                                       // Get hash code
    while (1) {
        if (vocabw_hash[hash] == -1) return -1;                                 // Word not found -> -1
        if (!strcmp(word, vocabw[vocabw_hash[hash]].word)) return vocabw_hash[hash];
        hash = (hash + 1) % vocab_hash_size;                                    // Hash collision -> hash + 1
    }
    return -1;                                                                  // Word not found -> -1
}

/**
 Index of stem in vocabulary.

 Native comment:
 Returns position of a stem in the vocabulary; if the stem is not found, returns -1
 */
int SearchVocabm(char *stem) {
    unsigned int hash = StringHash(stem);                                // Get hash code
    while (1) {
        if (vocabm_hash[hash] == -1) return -1;                          // Stem not found -> -1
        if (!strcmp(stem, vocabm[vocabm_hash[hash]].stem)) return vocabm_hash[hash];
        hash = (hash + 1) % vocab_hash_size;                             // Hash collision -> hash + 1
    }
    return -1;                                                           // Stem not found -> -1
}

/**
 Reads a word and returns its index in the vocabulary
 */
int ReadWordIndex(FILE *fin) {
    char word[MAX_STRING];
    ReadWord(word, fin);
    if (feof(fin)) return -1;
    return SearchVocabw(word);
}

/*
 * Reduces the vocabulary by removing infrequent tokens.
 *
 * After reduce must be called sort to initialize huffman codes!!
 */
void ReduceVocabs() {
    int a, b = 2;                                         // b=1 -> Skip UNKNOWN suffix, b=2 -> Skip NOSUF suffix
    unsigned int hash;
    for (a = 2; a < vocabs_size; a++)                     // a=1 -> Skip UNKNOWN suffix, b=2 -> Skip NOSUF suffix
        if (vocabs[a].cn > min_reduce) {                  // Reducing threshold
            vocabs[b].cn = vocabs[a].cn;                                                // Just shifting to the left!
            vocabs[b].suf = vocabs[a].suf;
            b++;
        } else free(vocabs[a].suf);                                                     // Free if smaller
    vocabs_size = b;                                                                    // Actual size of vocab
    for (a = 0; a < vocab_hash_size; a++) vocabs_hash[a] = -1;                          // Clear hash table
    for (a = 0; a < vocabs_size; a++) {                                                 // Recompute hash table
        // Hash will be re-computed, as it is not actual
        hash = StringHash(vocabs[a].suf);                                               // Suffix->hashCode()
        while (vocabs_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;            // Hash collision -> hash + 1
        vocabs_hash[hash] = a;                                                          // vocabs_hash[suffix->hashCode()] = a -> vocabs[a] = suffix
    }
    fflush(stdout);                                                                     // Flush debug messages
    min_reduce++;                                                                       // Increment threshold
}

/*
 * Reduces the vocabulary by removing infrequent tokens.
 *
 * After reduce must be called sort to initialize ngrams!!
 */
void ReduceVocabw() {
    int a, b = 1;                                           // b=1 -> Skip <s/> word
    unsigned int hash;
    for (a = 1; a < vocabw_size; a++)                       // a=1 -> Skip <s/> word
        if (vocabw[a].cn > min_reduce) {                    // Reducing threshold
            vocabw[b].cn = vocabw[a].cn;                                              // Just shifting to the left!
            vocabw[b].word = vocabw[a].word;
            b++;
        } else free(vocabw[a].word);                                                  // Free if smaller
    vocabw_size = b;                                                                  // Actual size of vocab
    for (a = 0; a < vocab_hash_size; a++) vocabw_hash[a] = -1;                        // Clear hash table
    for (a = 0; a < vocabw_size; a++) {                                               // Recompute hash table
        // Hash will be re-computed, as it is not actual
        hash = StringHash(vocabw[a].word);                                            // Word->hashCode()
        while (vocabw_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;          // Hash collision -> hash + 1
        vocabw_hash[hash] = a;                                                        // vocab_hash[word->hashCode()] = a -> vocabw[a] = word
    }
    fflush(stdout);                                                                   // Flush debug messages
    min_reduce++;                                                                     // Increment threshold
}

/*
 * Reduces the vocabulary by removing infrequent tokens.
 *
 * After reduce must be called sort to initialize huffman codes!!
 */
void ReduceVocabm() {
    int a, b = 1;                                         // b=1 -> Skip UNKNOWN stem
    unsigned int hash;
    for (a = 1; a < vocabm_size; a++)                     // a=1 -> Skip UNKNOWN stem
        if (vocabm[a].cn > min_reduce) {                  // Reducing threshold
            vocabm[b].cn = vocabm[a].cn;                                                // Just shifting to the left!
            vocabm[b].stem = vocabm[a].stem;
            b++;
        } else free(vocabm[a].stem);                                                    // Free if smaller
    vocabm_size = b;                                                                    // Actual size of vocab
    for (a = 0; a < vocab_hash_size; a++) vocabm_hash[a] = -1;                          // Clear hash table
    for (a = 0; a < vocabm_size; a++) {                                                 // Recompute hash table
        // Hash will be re-computed, as it is not actual
        hash = StringHash(vocabm[a].stem);                                              // Stem->hashCode()
        while (vocabm_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;            // Hash collision -> hash + 1
        vocabm_hash[hash] = a;                                                          // vocabm_hash[stem->hashCode()] = a -> vocabm[a] = stem
    }
    fflush(stdout);                                                                     // Flush debug messages
    min_reduce++;                                                                       // Increment threshold
}

/**
 Add a word to the vocabulary of words
 */
int AddWordToVocab(char *word) {
    unsigned int hash, length = strlen(word) + 1;
    if (length > MAX_STRING) length = MAX_STRING;
    vocabw[vocabw_size].word = (char *)calloc(length, sizeof(char));
    strcpy(vocabw[vocabw_size].word, word);
    vocabw[vocabw_size].cn = 0;
    vocabw_size++;
    // Reallocate memory if needed
    if (vocabw_size + 2 >= vocabw_max_size) {
        vocabw_max_size += 1000;
        vocabw = (struct vocab_word *)realloc(vocabw, vocabw_max_size * sizeof(struct vocab_word));
    }
    hash = StringHash(word);
    while (vocabw_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
    vocabw_hash[hash] = vocabw_size - 1;
    return vocabw_size - 1;
}

/**
 Add a suffix to the vocabulary of suffixes
 */
int AddSufToVocab(char *suf, int count) {
    unsigned int hash, pos, length = strlen(suf) + 1;
    if (length > MAX_STRING) length = MAX_STRING;
    pos = SearchVocabs(suf);
    if(pos == -1) {                                                                         // Not found add it to vocab
        vocabs[vocabs_size].suf = (char *)calloc(length, sizeof(char));
        strcpy(vocabs[vocabs_size].suf, suf);
        vocabs[vocabs_size].cn = count;
        pos = vocabs_size;
        vocabs_size++;
        // Reallocate memory if needed
        if (vocabs_size + 2 >= vocabs_max_size) {
            vocabs_max_size += 1000;
            vocabs = (struct vocab_suf *)realloc(vocabs, vocabs_max_size * sizeof(struct vocab_suf));
        }
        hash = StringHash(suf);
        while (vocabs_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
        vocabs_hash[hash] = pos;
    } else {                                                                                // Found, just increment counter
        vocabs[pos].cn += count;
    }

    return pos;
}

/**
 Add a stem to the vocabulary of stems
 */
int AddStemToVocab(char *stem, int count) {
    unsigned int hash, pos, length = strlen(stem) + 1;
    if (length > MAX_STRING) length = MAX_STRING;
    pos = SearchVocabm(stem);
    if(pos == -1) {                                                                         // Not found add it to vocab
        vocabm[vocabm_size].stem = (char *)calloc(length, sizeof(char));
        strcpy(vocabm[vocabm_size].stem, stem);
        vocabm[vocabm_size].cn = count;
        pos = vocabm_size;
        vocabm_size++;
        // Reallocate memory if needed
        if (vocabm_size + 2 >= vocabm_max_size) {
            vocabm_max_size += 1000;
            vocabm = (struct vocab_stem *)realloc(vocabm, vocabm_max_size * sizeof(struct vocab_stem));
        }
        hash = StringHash(stem);
        while (vocabm_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
        vocabm_hash[hash] = vocabm_size - 1;
    } else {                                                                                // Found, just increment counter
        vocabm[pos].cn += count;
    }
    return pos;
}

// Used later for sorting by word counts --> Descending!!!
int VocabsCompare(const void *a, const void *b) {
    return ((struct vocab_suf *)b)->cn - ((struct vocab_suf *)a)->cn;
}

// Used later for sorting by word counts --> Descending!!!
int VocabwCompare(const void *a, const void *b) {
    return ((struct vocab_word *)b)->cn - ((struct vocab_word *)a)->cn;
}

// Used later for sorting by word counts --> Descending!!!
int VocabmCompare(const void *a, const void *b) {
    return ((struct vocab_stem *)b)->cn - ((struct vocab_stem *)a)->cn;
}

// Sorts the vocabulary by frequency using word counts
void SortVocabw() {
    int a, size, pos, b = 1;
    unsigned int hash;
    char *tok, suf[MAX_STRING], stem[MAX_STRING], temp[MAX_STRING];
    // Sort the vocabulary and keep </s> at the first position
    qsort(&vocabw[1], vocabw_size - 1, sizeof(struct vocab_word), VocabwCompare);          // Quick sort on vocab according to occurrences
    for (a = 0; a < vocab_hash_size; a++) vocabw_hash[a] = -1;                             // Clear indices
    vocabw_hash[StringHash(vocabw[0].word)] = 0;
    size = vocabw_size;                                                                    // Vocab actual size
    train_words = 0;                                                                       // Train words initial value -> 0
    for (a = 1; a < size; a++) {
        vocabw[b].word = vocabw[a].word;                                                   // Move current word to true position
        vocabw[b].cn = vocabw[a].cn;
        if(vocabw[b].cn > min_count) {
            strcpy(temp, vocabw[b].word);                                                  // Create temp variable -> strtok is modifying source
            tok = strtok(temp, STEM_DELIM);                                                // Get stem
            if(tok == NULL) continue;                                                      // Mistake in preprocessed file
            strcpy(stem, tok);                                                             // Copy stem to prepared variable
            pos = SearchVocabm(stem);                                                      // Find stem
            if(pos == -1 && !unknown) vocabw[b].cn = 0;                                    // Stem not found and words with unknown stem should be skipped
            else {
                if(pos == -1) pos = SearchVocabm(UNKNOWN);                                 // Stem not found -> replace it with UNKNOWN stem
                vocabw[b].stem = pos;                                                      // Stem index
            }
            tok = strtok(NULL, STEM_DELIM);                                                // Continue with suffix
            if(tok != NULL) {                                                              // Suffix provided
                strcpy(suf, tok);                                                          // Copy suffix to prepared variable
                pos = SearchVocabs(suf);                                                   // Find suffix
                if(pos && !unknown) vocabw[b].cn = 0;                                      // Suffix not found and words with unknown suffix should be skipped
                else {
                    if(pos == -1) pos = SearchVocabs(UNKNOWN);                             // Suffix not found -> replace it with UNKNOWN suffix
                    vocabw[b].suf = pos;                                                   // Suffix index
                }
            } else {
                vocabw[b].suf = 1;                                                        // No suffix found at all -> word without suffix
            }
        }
        // Words occuring less than min_count times will be discarded from the vocab
        if ((vocabw[b].cn < min_count)) {                                      // Min count threshold
            vocabw_size--;                                                                 // Vocabulary is smaller
            free(vocabw[b].word);                                                          // Free memory of string -> characters
        } else {
            // Hash will be re-computed, as after the sorting it is not actual
            hash=StringHash(vocabw[b].word);                                               // Hashcode of word
            while (vocabw_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;           // Collision -> hash + 1
            vocabw_hash[hash] = b;                                                         // a = index in vocab -> vocab_hash[hash(word)] = a -> vocab[a] = word
            train_words += vocabw[b].cn;
            b++;
        }
    }
    vocabw_size = b;
    vocabw = (struct vocab_word *)realloc(vocabw, (vocabw_size + 1) * sizeof(struct vocab_word));    // Realloc memory to fit exactly the size of vocabulary!
    // Allocate memory for the binary tree construction
    for (a = 0; a < vocabw_size; a++) {
        vocabw[a].code = (char *)calloc(MAX_CODE_LENGTH, sizeof(char));                    // Alloc space for Huffman code
        vocabw[a].point = (int *)calloc(MAX_CODE_LENGTH, sizeof(int));                     // Alloc space for Parents
    }
}


// Sorts the vocabulary by frequency using suffix counts
void SortVocabs() {
    int a, size;
    unsigned int hash;
    // Sort the vocabulary and keep UNKNOWN suffix at the first position and NOSUF at the second position
    qsort(&vocabs[2], vocabs_size - 2, sizeof(struct vocab_suf), VocabsCompare);          // Quick sort on vocab according to occurrences
    for (a = 0; a < vocab_hash_size; a++) vocabs_hash[a] = -1;                             // Clear indices
    vocabs_hash[StringHash(vocabs[0].suf)] = 0;
    vocabs_hash[StringHash(vocabs[1].suf)] = 1;
    size = vocabs_size;                                                                    // Vocab actual size
    for (a = 2; a < size; a++) {
        // Words occuring less than min_count times will be discarded from the vocab
        if ((vocabs[a].cn < min_count)) {                                      // Min count threshold
            vocabs_size--;                                                                 // Vocabulary is smaller
            free(vocabs[a].suf);                                                          // Free memory of string -> characters
        } else {
            // Hash will be re-computed, as after the sorting it is not actual
            hash=StringHash(vocabs[a].suf);                                               // Hashcode of word
            while (vocabs_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;           // Collision -> hash + 1
            vocabs_hash[hash] = a;                                                         // a = index in vocab -> vocab_hash[hash(word)] = a -> vocab[a] = word
        }
    }
    vocabs = (struct vocab_suf *)realloc(vocabs, (vocabs_size + 1) * sizeof(struct vocab_suf));    // Realloc memory to fit exactly the size of vocabulary!
}

// Sorts the vocabulary by frequency using stem counts
void SortVocabm() {
    int a, size;
    unsigned int hash;
    // Sort the vocabulary and keep </s> at the first position
    qsort(&vocabm[1], vocabm_size - 1, sizeof(struct vocab_stem), VocabmCompare);          // Quick sort on vocab according to occurrences
    for (a = 0; a < vocab_hash_size; a++) vocabm_hash[a] = -1;                             // Clear indices
    vocabm_hash[StringHash(vocabm[0].stem)] = 0;
    size = vocabm_size;                                                                    // Vocab actual size
    for (a = 1; a < size; a++) {
        // Words occuring less than min_count times will be discarded from the vocab
        if ((vocabm[a].cn < min_count)) {                                                  // Min count threshold
            vocabm_size--;                                                                 // Vocabulary is smaller
            free(vocabm[a].stem);                                                          // Free memory of string -> characters
        } else {
            // Hash will be re-computed, as after the sorting it is not actual
            hash=StringHash(vocabm[a].stem);                                               // Hashcode of word
            while (vocabm_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;           // Collision -> hash + 1
            vocabm_hash[hash] = a;                                                         // a = index in vocab -> vocab_hash[hash(word)] = a -> vocab[a] = word
        }
    }
    vocabm = (struct vocab_stem *)realloc(vocabm, (vocabm_size + 1) * sizeof(struct vocab_stem));    // Realloc memory to fit exactly the size of vocabulary!
}

/**
 Creates Huffman tree!

 // Create binary Huffman tree using the word counts
 // Frequent words will have short uniqe binary codes
 */
void CreateBinaryTree() {
    long long a, b, i, min1i, min2i, pos1, pos2, point[MAX_CODE_LENGTH];                       // Help variables
    char code[MAX_CODE_LENGTH];                                                                // Current code
    long long *count = (long long *)calloc(vocabw_size * 2 + 1, sizeof(long long));            // Word->cn quick reference
    long long *binary = (long long *)calloc(vocabw_size * 2 + 1, sizeof(long long));           // 1 or 0
    long long *parent_node = (long long *)calloc(vocabw_size * 2 + 1, sizeof(long long));      // Parent indices
    for (a = 0; a < vocabw_size; a++) count[a] = vocabw[a].cn;                                 // Copy occurrence counts
    for (a = vocabw_size; a < vocabw_size * 2; a++) count[a] = 1e15;                           // Fill rest with big numbers -> We will look for minimum
    pos1 = vocabw_size - 1;                                                                    // In the middle - 1
    pos2 = vocabw_size;                                                                        // In the middle
    // Following algorithm constructs the Huffman tree by adding one node at a time
    for (a = 0; a < vocabw_size - 1; a++) {                                                    // Go through vocabulary
        // First, find two smallest nodes 'min1, min2'
        if (pos1 >= 0) {                                          // |<-- left edge
            if (count[pos1] < count[pos2]) {                      // left < right
                min1i = pos1;                                     // Left min = left index
                pos1--;                                           // <-- go left <-- left index
            } else {                                              // left > right
                min1i = pos2;                                     // Left min = right index
                pos2++;                                           // --> go right --> right index
            }
        } else {                                                  // What to do on the edge
            min1i = pos2;                                         // Left min = right index
            pos2++;                                               // --> go right --> right index
        }
        if (pos1 >= 0) {                                          // Just looking for second min
            if (count[pos1] < count[pos2]) {
                min2i = pos1;
                pos1--;
            } else {
                min2i = pos2;
                pos2++;
            }
        } else {
            min2i = pos2;
            pos2++;
        }
        count[vocabw_size + a] = count[min1i] + count[min2i];    // Filling empty counts on the right side -> filled with summary of two least used words
        parent_node[min1i] = vocabw_size + a;                    // Going up In the tree
        parent_node[min2i] = vocabw_size + a;                    // Parent is same for both children
        binary[min2i] = 1;                                       // Going right -> 1, Going left -> 0 (Prefilled with zeros)
    }

    // Now assign binary code to each vocabulary word
    for (a = 0; a < vocabw_size; a++) {                             // a -> Current word in vocab
        b = a;                                                      // b -> Current node in tree
        i = 0;                                                      // start of codes and points
        while (1) {                                                 // Infinite loop
            code[i] = binary[b];                                    // Add code mark according to current node (parent)
            point[i] = b;                                           // Decision node in tree (index)
            i++;                                                    // Next code / point
            b = parent_node[b];                                     // Go to parent
            if (b == vocabw_size * 2 - 2) break;                    // Root reached!
        }
        vocabw[a].codelen = i;                                      // Codelength = number of parents in tree = depth of node
        vocabw[a].point[0] = vocabw_size - 2;                       // Point[0] = root -> also removed vocab_size offset
        for (b = 0; b < i; b++) {                                   // Reverse filling of array
            vocabw[a].code[i - b - 1] = code[b];                    // Assign code to word
            vocabw[a].point[i - b] = point[b] - vocabw_size;        // Just remove vocab_size offset
        }
    }
    free(count);              // Free temp references
    free(binary);
    free(parent_node);
}

/**
 Create vocabulary from input file.
 */
void LearnVocabFromTrainFile() {
    char word[MAX_STRING], stem[MAX_STRING], suf[MAX_STRING], *tok;    // Space for actual word
    FILE *fin;                                                         // Input file
    int i;
    long long a;
    for (a = 0; a < vocab_hash_size; a++) vocabs_hash[a] = -1;         // Clear hash table
    for (a = 0; a < vocab_hash_size; a++) vocabw_hash[a] = -1;         // Clear hash table
    for (a = 0; a < vocab_hash_size; a++) vocabm_hash[a] = -1;         // Clear hash table
    fin = fopen(train_file, "rb");                                     // Input file = train file
    if (fin == NULL) {                                                 // Not found
        printf("ERROR: training data file not found!\n");
        exit(1);
    }
    vocabw_size = vocabs_size = vocabm_size = 0;                        // Actual vocabulary size
    AddWordToVocab((char *)"</s>");                                     // Add </s> \n char to vocabulary
    AddStemToVocab(UNKNOWN, 1000000);                                   // Add UNKNOWN suffix to vocabulary
    AddSufToVocab(UNKNOWN, 1000000);                                    // Add UNKNOWN stem to vocabulary
    AddSufToVocab(NOSUF, 1000000);                                      // Add NOSUF suffix to vocabulary
    while (1) {                                                         // Just loop
        ReadWord(word, fin);                                            // word = next word
        if (feof(fin)) break;                                           // Just stop looping
        train_words++;
        if ((debug_mode > 1) && (train_words % 100000 == 0)) {          // Debug print
            printf("%lldK%c", train_words / 1000, 13);
            fflush(stdout);
        }
        i = SearchVocabw(word);                                         // Search word
        if (i == -1) {                                                  // Word not found -> add it
            a = AddWordToVocab(word);
            vocabw[a].cn = 1;
        } else vocabw[i].cn++;                                          // Word found -> increment counter
        if (vocabw_size > vocab_hash_size * 0.7) ReduceVocabw();        // Too many words in vocab
        tok = strtok(word, STEM_DELIM);                                 // Get stem
        if(tok == NULL) continue;                                       // Mistake in preprocessed file
        strcpy(stem, tok);                                              // Copy stem to prepared variable
        AddStemToVocab(stem, 1);                                        // Add stem to vocab
        if (vocabm_size > vocab_hash_size * 0.7) ReduceVocabm();        // Too many stems in vocab
        tok = strtok(NULL, STEM_DELIM);                                 // Get suffix
        if(tok != NULL) {                                               // Does the word have suffix?
            strcpy(suf, tok);                                           // Copy suffix into prepared variable
            AddSufToVocab(suf, 1);                                      // Add suffix to vocab
            if (vocabs_size > vocab_hash_size * 0.7) ReduceVocabs();    // Too many suffixes in vocabulary
        }
    }
    SortVocabm();                                                       // Sort the vocabs
    SortVocabs();
    SortVocabw();
    if (debug_mode > 2) printWord2stemsuf();
    if (debug_mode > 0) {                                               // Debug print
        printf("Word Vocab size: %lld\n", vocabw_size);
        printf("Stem Vocab size: %lld\n", vocabm_size);
        printf("Suffix Vocab size: %lld\n", vocabs_size);
        printf("Words in train file: %lld\n", train_words);
    }
    file_size = ftell(fin);                                             // Save file size
    fclose(fin);                                                        // Close stream
}

/**
 Save created vocabulary in file. Text representation
 */
void SaveVocab() {
    long long i;
    FILE *fo = fopen(save_vocab_file, "wb");
    for (i = 0; i < vocabw_size; i++) fprintf(fo, "%s %lld\n", vocabw[i].word, vocabw[i].cn);
    fclose(fo);
}

/**
 Read vocabulary from file.
 */
void ReadVocab() {
    long long a;
    char c;
    char word[MAX_STRING], *tok, stem[MAX_STRING], suf[MAX_STRING], temp[MAX_STRING];
    FILE *fin = fopen(read_vocab_file, "rb");
    if (fin == NULL) {
        printf("Vocabulary file not found\n");
        exit(1);
    }
    for (a = 0; a < vocab_hash_size; a++) vocabm_hash[a] = -1;         // Clear hash table
    for (a = 0; a < vocab_hash_size; a++) vocabw_hash[a] = -1;         // Clear hash table
    for (a = 0; a < vocab_hash_size; a++) vocabs_hash[a] = -1;         // Clear hash table
    vocabw_size = vocabs_size = vocabm_size = 0;                       // Initial size of vocabs
    while (1) {
        ReadWord(word, fin);                                           // Read next word
        if (feof(fin)) break;                                          // EOF -> end
        a = AddWordToVocab(word);                                      // Add read word to vocab
        fscanf(fin, "%lld%c", &vocabw[a].cn, &c);                      // Scan the occurences
        strcpy(temp, vocabw[a].word);                                  // Copy word into temp variable -> strtok is modifying source
        tok = strtok(temp, STEM_DELIM);                                // Get stem
        if(tok == NULL) continue;                                      // Wrong word format
        strcpy(stem, tok);                                             // Copy stem into prepared variable
        AddStemToVocab(stem, vocabw[a].cn);                            // Add stem in vocab
        if (vocabm_size > vocab_hash_size * 0.7) ReduceVocabm();       // Too many stems in vocab
        tok = strtok(NULL, STEM_DELIM);                                // Get suffix
        if(tok != NULL) {                                              // Does the word have suffix?
            strcpy(suf, tok);                                          // Copy suffix into prepared variable
            AddSufToVocab(suf, vocabw[a].cn);                          // Add suffix in vocab
            if (vocabs_size > vocab_hash_size * 0.7) ReduceVocabs();   // Too many suffixes in vocab
        }
    }
    SortVocabs();                                                      // Re-sort vocabulary
    SortVocabm();
    SortVocabw();                                                      // WARNING! MUST BE LAST ONE SORTING!!!
    if (debug_mode > 2) printWord2stemsuf();
    if (debug_mode > 0) {                                              // Debug print
        printf("Word Vocab size: %lld\n", vocabw_size);
        printf("Stem Vocab size: %lld\n", vocabm_size);
        printf("Suffix Vocab size: %lld\n", vocabs_size);
        printf("Words in train file: %lld\n", train_words);
    }
    fin = fopen(train_file, "rb");
    if (fin == NULL) {
        printf("ERROR: training data file not found!\n");
        exit(1);
    }
    fseek(fin, 0, SEEK_END);
    file_size = ftell(fin);
    fclose(fin);
}

/**
 Initialize neural network.
 */
void InitNet() {
    long long a, b;
    unsigned long long next_random = 1;
    a = posix_memalign((void **)&syn0, 128, (long long) (vocabw_size + vocabm_size) * layer1_size * sizeof(real)); // Input -> Projection weights - vocab_size x layer1_size
    if (syn0 == NULL) {printf("Memory allocation failed\n"); exit(1);}
    if (hs) {                                                                                                      // HS: Projection -> Output weights - vocab_size x layer1_size
        a = posix_memalign((void **)&syn1, 128, (long long)vocabw_size * layer1_size * sizeof(real));
        if (syn1 == NULL) {printf("Memory allocation failed\n"); exit(1);}
        for (a = 0; a < vocabw_size; a++) for (b = 0; b < layer1_size; b++) syn1[a * layer1_size + b] = 0;         // Filled with zeros
    }
    if (negative>0) {                                                                                              // NEG: Projection -> Output weights - vocab_size x layer1_size
        a = posix_memalign((void **)&syn1neg, 128, (long long)vocabw_size * layer1_size * sizeof(real));
        if (syn1neg == NULL) {printf("Memory allocation failed\n"); exit(1);}
        for (a = 0; a < vocabw_size; a++) for (b = 0; b < layer1_size; b++) syn1neg[a * layer1_size + b] = 0;      // Filled with zeros
    }
    for (a = 0; a < (vocabw_size + vocabm_size); a++) for (b = 0; b < layer1_size; b++) {
            next_random = next_random * (unsigned long long)25214903917 + 11;
            syn0[a * layer1_size + b] = (((next_random & 0xFFFF) / (real)65536) - 0.5) / layer1_size;              // Fill initial (Input -> Projection) weights -> (random number < 0.5) / layer_size
        }
    CreateBinaryTree();                                                                                            // Create the Huffman tree
}

/**
 Train neural network! Main method.

 Thread runnable method.
 */
void *TrainModelThread(void *id) {
    long long a, b, d, cw, word, last_word, sentence_length = 0, sentence_position = 0; // Helpers
    long long word_count = 0, last_word_count = 0, sen[MAX_SENTENCE_LENGTH + 1];        // Helpers
    long long l2, c, target, label, local_iter = iter;                                  // Helpers
    int i;
    unsigned long long next_random = (long long)id;                                     // Random generator seed -> id of thread
    real f;                                                                             // Output value
    real g;                                                                             // Gradient value
    clock_t now;                                                                        // Time helper
    real *neu1 = (real *)calloc(layer1_size, sizeof(real));                             // Projection layer
    real *neu1e = (real *)calloc(layer1_size, sizeof(real));                            // Projection layer errors
    long long woff = vocabw_size * layer1_size;
    FILE *fi = fopen(train_file, "rb");                                                 // Open train file
    fseek(fi, file_size / (long long)num_threads * (long long)id, SEEK_SET);            // Load assigned data
    while (1) {                                                                         // While not EOF
        if (word_count - last_word_count > 10000) {                                     // Just printing information
            word_count_actual += word_count - last_word_count;                          // Get number for this step
            last_word_count = word_count;                                               // Renew last word count
            if ((debug_mode > 1)) {                                                     // If debug on -> print
                now=clock();                                                            // Save time
                printf("%cAlpha: %f  Progress: %.2f%%  Words/thread/sec: %.2fk  ", 13, alpha, word_count_actual / (real)(iter * train_words + 1) * 100, word_count_actual / ((real)(now - start + 1) / (real)CLOCKS_PER_SEC * 1000));
                fflush(stdout);                                                         // Print statistics
            }
            alpha = starting_alpha * (1 - word_count_actual / (real)(iter * train_words + 1));   // Make smaller learning alpha
            if (alpha < starting_alpha * 0.0001) alpha = starting_alpha * 0.0001;         // If alfa is too small
        }
        if (sentence_length == 0) {                                                       // Sentence not loaded
            while (1) {
                word = ReadWordIndex(fi);                                                 // Read word from line
                if (feof(fi)) break;                                                      // EOF -> End of main loop
                if (word == -1) continue;                                                 // Unknown word
                word_count++;
                if (word == 0) break;                                                     // 0 -> <s> -> End of line -> Sentence loaded
                // The subsampling randomly discards frequent words while keeping the ranking same
                if (sample > 0) {                                                         // Subsampling on!
                    real ran = (sqrt(vocabw[word].cn / (sample * train_words)) + 1) * (sample * train_words) / vocabw[word].cn;     // Generate threshold value
                    next_random = next_random * (unsigned long long)25214903917 + 11;                                               // Generate next random value
                    if (ran < (next_random & 0xFFFF) / (real)65536) continue;                                                       // Compare generated threshold with random value
                }
                sen[sentence_length] = word;                                              // Save word into sentence
                sentence_length++;                                                        // Increment sentence length
                if (sentence_length >= MAX_SENTENCE_LENGTH) break;                        // Cut too long sentences -> there is a limit
            }
            sentence_position = 0;                                                        // Starting with first word
        }
        if (feof(fi) || (word_count > train_words / num_threads)) {
            word_count_actual += word_count - last_word_count;
            local_iter--;
            if (local_iter == 0) break;
            word_count = 0;
            last_word_count = 0;
            sentence_length = 0;
            fseek(fi, file_size / (long long) num_threads * (long long) id, SEEK_SET);
            continue;
        }
        word = sen[sentence_position];                                              // Get current word
        if (word == -1) continue;                                                   // Unknown word -> probably just check (unknown words should be already skipped)
        for (c = 0; c < layer1_size; c++) neu1[c] = 0;                              // Projection layer is cleared for every word
        for (c = 0; c < layer1_size; c++) neu1e[c] = 0;                             // Errors for Projection layer are cleared for every word
        next_random = next_random * (unsigned long long)25214903917 + 11;           // Generate next random value
        b = next_random % window;                                                   // Set random size of window
        if (cbow) {  //train the cbow architecture
            // in -> Projection
            cw = 0;
            for (a = b; a < window * 2 + 1 - b; a++) {
                if (a != window) {                                                  // Clever loop for iterating over random size window!
                    c = sentence_position - window + a;
                    if (c < 0) continue;
                    if (c >= sentence_length) continue;
                    last_word = sen[c];                                                     // Current word in window in sentence
                    if (last_word == -1) continue;                                          // Not in dictionary
                    for(c = 0; c < layer1_size; c++) neu1[c] += syn0[c + last_word * layer1_size];             // Copy word vector
                    cw++;
                }
            }
            if(cw) {
                for (c = 0; c < layer1_size; c++) neu1[c] /= cw;
                if (hs) {
                    for (i = 0; i < vocabw[word].codelen; i++) {
                        f = 0;                                                                  // Output value set to 0
                        l2 = vocabw[word].point[i] * layer1_size;                          // Find decision node row = l2
                        // Propagate Projection -> output
                        for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1[c + l2];          // Projection * Projection-output weight
                        if (f <= -MAX_EXP) continue;                                            // If smaller than minumum go for next code
                        else if (f >= MAX_EXP) continue;                                        // If bigger than maximum go for next code
                        else f = expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];   // Just fit f to range (0, 2*MAX_EXP)
                        // 'g' is the gradient multiplied by the learning rate
                        g = (1 - vocabw[word].code[i] - f) * alpha;                              // Gradient = (1|0 - f * alpha) -> 1 -> -f && 0 -> +f
                        // Propagate errors output -> Projection
                        for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1[c + l2];         // Update errors
                        // Learn weights Projection -> output
                        for (c = 0; c < layer1_size; c++) syn1[c + l2] += g * neu1[c];          // Update weights
                    }
                }
                // NEGATIVE SAMPLING
                if (negative > 0)
                    for (d = 0; d < negative + 1; d++) {                    // Iterate over number of negative sampling
                        if (d == 0) {                                                             // D == 0 -> Current word
                            target = word;                                                        // Target is current word
                            label = 1;                                                            // Label = 1 -> Current word
                        } else {
                            next_random = next_random * (unsigned long long) 25214903917 + 11;     // Generate next random value
                            target = table[(next_random >> 16) % table_size];                     // Get random value from table
                            if (target == 0) target = next_random % (vocabw_size - 1) + 1;         // If target == 0 -> Get random word from vocabulary
                            if (target == word) continue;                                         // If target == current word -> Continue
                            label = 0;                                                            // Label = 0 -> Not current word
                        }
                        l2 = target * layer1_size;                             // Layer of target word
                        f = 0;                                                                  // Output value set to 0
                        for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1neg[c + l2];       // Projection * Projection-output weight
                        if (f > MAX_EXP) g = (label - 1) * alpha;                               // If bigger -> fixed gradient -> label 1: 0, label 0: -alpha
                        else if (f < -MAX_EXP) g = (label - 0) * alpha;                         // If smaller -> fixed gradient -> label 1: alpha, label 0: 0
                        else g = (label - expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;   // Gradient = (1|0 - f * alpha) -> 1 -> -f && 0 -> +f
                        for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1neg[c + l2];      // Update errors
                        for (c = 0; c < layer1_size; c++) syn1neg[c + l2] += g * neu1[c];       // Update weights
                    }
                // Projection -> in
                for (a = b; a < window * 2 + 1 - b; a++)
                    if (a != window) {                          // Clever loop for iterating over random size window! Size is same as for in -> Projection process!!!
                        c = sentence_position - window + a;
                        if (c < 0) continue;
                        if (c >= sentence_length) continue;
                        last_word = sen[c];
                        if (last_word == -1) continue;
                        for (c = 0; c < layer1_size; c++) syn0[c + last_word * layer1_size] += neu1e[c];                                           // Propagate to word vector
                    }
            }

            // Clear projection layer
            for(c = 0; c < layer1_size; c++) neu1[c] = 0;
            for(c = 0; c < layer1_size; c++) neu1e[c] = 0;
            cw = 0;

            for (a = b; a < window * 2 + 1 - b; a++) {
                if (a != window) {                                                  // Clever loop for iterating over random size window!
                    c = sentence_position - window + a;
                    if (c < 0) continue;
                    if (c >= sentence_length) continue;
                    last_word = sen[c];                                                     // Current word in window in sentence
                    if (last_word == -1) continue;                                          // Not in dictionary
                    for (c = 0; c < layer1_size; c++) neu1[c] += syn0[c + vocabw[last_word].stem * layer1_size + woff];   // Copy stem vector
                    cw++;
                }
            }
            if (cw) {
                for (c = 0; c < layer1_size; c++) neu1[c] /= cw;
                if (hs) {
                    for (i = 0; i < vocabw[word].codelen; i++) {
                        f = 0;                                                                  // Output value set to 0
                        l2 = vocabw[word].point[i] * layer1_size;                          // Find decision node row = l2
                        // Propagate Projection -> output
                        for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1[c + l2];          // Projection * Projection-output weight
                        if (f <= -MAX_EXP) continue;                                            // If smaller than minumum go for next code
                        else if (f >= MAX_EXP) continue;                                        // If bigger than maximum go for next code
                        else f = expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];   // Just fit f to range (0, 2*MAX_EXP)
                        // 'g' is the gradient multiplied by the learning rate
                        g = (1 - vocabw[word].code[i] - f) * alpha;                              // Gradient = (1|0 - f * alpha) -> 1 -> -f && 0 -> +f
                        // Propagate errors output -> Projection
                        for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1[c + l2];         // Update errors
                        // Learn weights Projection -> output
                        for (c = 0; c < layer1_size; c++) syn1[c + l2] += g * neu1[c];          // Update weights
                    }
                }
                // NEGATIVE SAMPLING
                if (negative > 0)
                    for (d = 0; d < negative + 1; d++) {                    // Iterate over number of negative sampling
                        if (d == 0) {                                                             // D == 0 -> Current word
                            target = word;                                                        // Target is current word
                            label = 1;                                                            // Label = 1 -> Current word
                        } else {
                            next_random = next_random * (unsigned long long) 25214903917 + 11;     // Generate next random value
                            target = table[(next_random >> 16) % table_size];                     // Get random value from table
                            if (target == 0) target = next_random % (vocabw_size - 1) + 1;         // If target == 0 -> Get random word from vocabulary
                            if (target == word) continue;                                         // If target == current word -> Continue
                            label = 0;                                                            // Label = 0 -> Not current word
                        }
                        l2 = target * layer1_size;                             // Layer of target word
                        f = 0;                                                                  // Output value set to 0
                        for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1neg[c + l2];       // Projection * Projection-output weight
                        if (f > MAX_EXP) g = (label - 1) * alpha;                               // If bigger -> fixed gradient -> label 1: 0, label 0: -alpha
                        else if (f < -MAX_EXP) g = (label - 0) * alpha;                         // If smaller -> fixed gradient -> label 1: alpha, label 0: 0
                        else g = (label - expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;   // Gradient = (1|0 - f * alpha) -> 1 -> -f && 0 -> +f
                        for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1neg[c + l2];      // Update errors
                        for (c = 0; c < layer1_size; c++) syn1neg[c + l2] += g * neu1[c];       // Update weights
                    }
                // Projection -> in
                for (a = b; a < window * 2 + 1 - b; a++)
                    if (a != window) {                          // Clever loop for iterating over random size window! Size is same as for in -> Projection process!!!
                        c = sentence_position - window + a;
                        if (c < 0) continue;
                        if (c >= sentence_length) continue;
                        last_word = sen[c];
                        if (last_word == -1) continue;
                        for (c = 0; c < layer1_size; c++) syn0[c + vocabw[last_word].stem * layer1_size + woff] += neu1e[c];     // Propagate to stem vector
                    }
            }
        } else {  //train skip-gram
            for (a = b; a < window * 2 + 1 - b; a++)
                if (a != window) {                              // Clever loop for iterating over random size window!
                    c = sentence_position - window + a;
                    if (c < 0) continue;
                    if (c >= sentence_length) continue;
                    last_word = sen[c];
                    if (last_word == -1) continue;

                    // Word
                    for (c = 0; c < layer1_size; c++) neu1[c] = 0;                                  // Clear projection layer -> Each word has its own projection
                    for (c = 0; c < layer1_size; c++) neu1e[c] = 0;                                 // Clear projection layer errors for each word
                    for(c = 0; c < layer1_size; c++) neu1[c] += syn0[c + last_word * layer1_size];                                                 // Copy word vector
                    // HIERARCHICAL SOFTMAX
                    if (hs) {
                        for (d = 0; d < vocabw[word].codelen; d++) {
                            f = 0;                                                              // Output value set to 0
                            l2 = vocabw[word].point[d] * layer1_size;                      // Current code mark row  -> offset
                            // Propagate Projection -> output
                            for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1[c + l2]; // Output value
                            if (f <= -MAX_EXP) continue;
                            else if (f >= MAX_EXP) continue;
                            else f = expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];
                            // 'g' is the gradient multiplied by the learning rate
                            g = (1 - vocabw[last_word].code[d] - f) * alpha;
                            // Propagate errors output -> Projection
                            for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1[c + l2];
                            // Learn weights Projection -> output
                            for (c = 0; c < layer1_size; c++) syn1[c + l2] += g * neu1[c];
                        }
                    }
                    // NEGATIVE SAMPLING
                    if (negative > 0)
                        for (d = 0; d < negative + 1; d++) {
                            if (d == 0) {
                                target = word;
                                label = 1;
                            } else {
                                next_random = next_random * (unsigned long long)25214903917 + 11;
                                target = table[(next_random >> 16) % table_size];
                                if (target == 0) target = next_random % (vocabw_size - 1) + 1;
                                label = 0;
                            }
                            l2 = target * layer1_size;
                            f = 0;
                            for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1neg[c + l2];
                            if (f > MAX_EXP) g = (label - 1) * alpha;
                            else if (f < -MAX_EXP) g = (label - 0) * alpha;
                            else g = (label - expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;
                            for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1neg[c + l2];
                            for (c = 0; c < layer1_size; c++) syn1neg[c + l2] += g * neu1[c];
                        }
                    // Learn weights input -> Projection
                    for(c = 0; c < layer1_size; c++) syn0[c + last_word * layer1_size] += neu1e[c];                                                // Propagate to word vector

                    // Stem
                    for (c = 0; c < layer1_size; c++) neu1[c] = 0;                                  // Clear projection layer -> Each word has its own projection
                    for (c = 0; c < layer1_size; c++) neu1e[c] = 0;                                 // Clear projection layer errors for each word
                    for (c = 0; c < layer1_size; c++) neu1[c] += syn0[c + vocabw[last_word].stem * layer1_size + woff];           // Copy stem vector
                    if (hs) {
                        for (d = 0; d < vocabw[word].codelen; d++) {
                            f = 0;                                                              // Output value set to 0
                            l2 = vocabw[word].point[d] * layer1_size;                      // Current code mark row  -> offset
                            // Propagate Projection -> output
                            for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1[c + l2]; // Output value
                            if (f <= -MAX_EXP) continue;
                            else if (f >= MAX_EXP) continue;
                            else f = expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];
                            // 'g' is the gradient multiplied by the learning rate
                            g = (1 - vocabw[last_word].code[d] - f) * alpha;
                            // Propagate errors output -> Projection
                            for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1[c + l2];
                            // Learn weights Projection -> output
                            for (c = 0; c < layer1_size; c++) syn1[c + l2] += g * neu1[c];
                        }
                    }
                    // NEGATIVE SAMPLING
                    if (negative > 0)
                        for (d = 0; d < negative + 1; d++) {
                            if (d == 0) {
                                target = word;
                                label = 1;
                            } else {
                                next_random = next_random * (unsigned long long) 25214903917 + 11;
                                target = table[(next_random >> 16) % table_size];
                                if (target == 0) target = next_random % (vocabw_size - 1) + 1;
                                label = 0;
                            }
                            l2 = target * layer1_size;
                            f = 0;
                            for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1neg[c + l2];
                            if (f > MAX_EXP) g = (label - 1) * alpha;
                            else if (f < -MAX_EXP) g = (label - 0) * alpha;
                            else g = (label - expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;
                            for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1neg[c + l2];
                            for (c = 0; c < layer1_size; c++) syn1neg[c + l2] += g * neu1[c];
                        }
                    // Learn weights input -> Projection
                    for (c = 0; c < layer1_size; c++) syn0[c + vocabw[last_word].stem * layer1_size + woff] += neu1e[c];          // Propagate to stem vector
                }
        }
        sentence_position++;                                                    // Let's go for next word!!!
        if (sentence_position >= sentence_length) {                             // We are finally at the end of processing sentence
            sentence_length = 0;                                                // Let's go for a new one.
            continue;                                                           // Continue main loop
        }
    }
    fclose(fi);                                                                 // Clean up the mess.
    free(neu1);
    free(neu1e);
    pthread_exit(NULL);                                                         // Everything is done
}

/**
 Train the neural network. Run all threads!
 */
void TrainModel() {
    char *tok;
    long a, b, c, d;                                                                      // Helpers
    FILE *fo;                                                                             // Output file
    pthread_t *pt = (pthread_t *)malloc(num_threads * sizeof(pthread_t));                 // Prepare threads
    printf("Starting training using file %s\n", train_file);                              // Starting process
    starting_alpha = alpha;                                                               // Initial alpha
    if (read_vocab_file[0] != 0) ReadVocab(); else LearnVocabFromTrainFile();             // Read vocabulary if possible
    if (save_vocab_file[0] != 0) SaveVocab();                                             // Save created vocabulary if needed
    if (output_file[0] == 0) return;                                                      // No output file
    InitNet();                                                                            // Init net
    if (negative > 0) InitUnigramTable();                                                 // Init unigram table if negative sampling
    start = clock();                                                                      // Start counting time
    for (a = 0; a < num_threads; a++) pthread_create(&pt[a], NULL, TrainModelThread, (void *)a);  // Create threads
    for (a = 0; a < num_threads; a++) pthread_join(pt[a], NULL);                                  // Wait for threads
    fo = fopen(output_file, "wb");                                                        // Print results

    if (classes == 0) {                                                                                             // No classification classes
        // Save the word vectors
        fprintf(fo, "%lld %lld\n", vocabw_size, layer1_size);
        for (a = 0; a < vocabw_size; a++) {
            strtok(vocabw[a].word, STEM_DELIM);                                                                     // Stem
            tok = strtok(NULL, STEM_DELIM);                                                                         // Suffix
            if(tok != NULL) fprintf(fo, "%s%s ", vocabw[a].word, tok);                                              // Suffix exists -> last token + remaining after strtok
            else fprintf(fo, "%s ", vocabw[a].word);                                                                // Suffix does not exist -> just remaining after strtok
            if (binary) for (b = 0; b < layer1_size; b++) fwrite(&syn0[a * layer1_size + b], sizeof(real), 1, fo);  // Save binary (input -> Projection) weights
            else for (b = 0; b < layer1_size; b++) fprintf(fo, "%lf ", syn0[a * layer1_size + b]);                  // Save text (input -> Projection) weights
            fprintf(fo, "\n");
        }
    } else {
        // Run K-means on the word vectors
        int clcn = classes, iter = 10, closeid;
        int *centcn = (int *)malloc(classes * sizeof(int));                               // Classes counts
        int *cl = (int *)calloc(vocabw_size, sizeof(int));                               // Classes inclusion
        real closev, x;                                                                  // Helpers
        real *cent = (real *)calloc(classes * layer1_size, sizeof(real));                // Classes vectors
        for (a = 0; a < vocabw_size; a++) cl[a] = a % clcn;                              // Initial classes inclusion -> simple, just position % number of classes
        for (a = 0; a < iter; a++) {                                                     // Iterate over number of iteration
            for (b = 0; b < clcn * layer1_size; b++) cent[b] = 0;                        // Set classes vectors to 0
            for (b = 0; b < clcn; b++) centcn[b] = 1;                                    // Set classes count to 1
            for (c = 0; c < vocabw_size; c++) {                                          // Iterate over all words in vocab
                for (d = 0; d < layer1_size; d++) cent[layer1_size * cl[c] + d] += syn0[c * layer1_size + d];          // Add word vectors to corresponding classes vectors
                centcn[cl[c]]++;                                                         // Class distribution
            }
            for (b = 0; b < clcn; b++) {                                                 // Iterate over classes
                closev = 0;                                                              // Normalization helper
                for (c = 0; c < layer1_size; c++) {                                      // Iterate vectors
                    cent[layer1_size * b + c] /= centcn[b];                              // Make average values over all corresponding words
                    closev += cent[layer1_size * b + c] * cent[layer1_size * b + c];     // Summarize of second powers
                }
                closev = sqrt(closev);                                                   // sqrt(E a^2)
                for (c = 0; c < layer1_size; c++) cent[layer1_size * b + c] /= closev;   // Another normalization
            }
            for (c = 0; c < vocabw_size; c++) {                                          // Assign new classes
                closev = -10;                                                            // Maximum value for class
                closeid = 0;                                                             // Closest class number
                for (d = 0; d < clcn; d++) {                                             // Iterate over classes
                    x = 0;                                                               // Clear current value
                    for (b = 0; b < layer1_size; b++) x += cent[layer1_size * d + b] * syn0[c * layer1_size + b]; // Summarize class vector with word vector
                    if (x > closev) {                                                    // If more closer class
                        closev = x;                                                      // New maximum value
                        closeid = d;                                                     // Closest class number
                    }
                }
                cl[c] = closeid;                                                         // Assign best class for word
            }
        }
        // Save the K-means classes
        for (a = 0; a < vocabw_size; a++) fprintf(fo, "%s %d\n", vocabw[a].word, cl[a]);
        free(centcn);
        free(cent);
        free(cl);
    }
    fclose(fo);
}

/**
 Helper function for parsing arguments!
 */
int ArgPos(char *str, int argc, char **argv) {
    int a;
    for (a = 1; a < argc; a++)
        if (!strcmp(str, argv[a])) {
            if (a == argc - 1) {
                printf("Argument missing for %s\n", str);
                exit(1);
            }
            return a;
        }
    return -1;
}

/**
 Main function.
 */
int main(int argc, char **argv) {
    int i;
    if (argc == 1) {
        printf("WordStem with separated projection layer Vector estimation toolkit v1.0\n\n");
        printf("Options:\n");
        printf("Parameters for training:\n");
        printf("\t-train <file>\n");
        printf("\t\tUse text data from <file> to train the model\n");
        printf("\t-output <file>\n");
        printf("\t\tUse <file> to save the resulting word vectors / word clusters\n");
        printf("\t-size <int>\n");
        printf("\t\tSet size of word vectors; default is 100\n");
        printf("\t-window <int>\n");
        printf("\t\tSet max skip length between words; default is 5\n");
        printf("\t-sample <float>\n");
        printf("\t\tSet threshold for occurrence of words. Those that appear with higher frequency in the training data\n");
        printf("\t\twill be randomly down-sampled; default is 1e-3, useful range is (0, 1e-5)\n");
        printf("\t-hs <int>\n");
        printf("\t\tUse Hierarchical Softmax; default is 0 (not used)\n");
        printf("\t-negative <int>\n");
        printf("\t\tNumber of negative examples; default is 5, common values are 3 - 10 (0 = not used)\n");
        printf("\t-threads <int>\n");
        printf("\t\tUse <int> threads (default 1)\n");
        printf("\t-iter <int>\n");
        printf("\t\tRun more training iterations (default 5)\n");
        printf("\t-min-count <int>\n");
        printf("\t\tThis will discard words that appear less than <int> times; default is 5\n");
        printf("\t-alpha <float>\n");
        printf("\t\tSet the starting learning rate; default is 0.025 for skip-gram and 0.05 for CBOW\n");
        printf("\t-classes <int>\n");
        printf("\t\tOutput word classes rather than word vectors; default number of classes is 0 (vectors are written)\n");
        printf("\t-debug <int>\n");
        printf("\t\tSet the debug mode (default = 2 = more info during training)\n");
        printf("\t-binary <int>\n");
        printf("\t\tSave the resulting vectors in binary moded; default is 0 (off)\n");
        printf("\t-save-vocab <file>\n");
        printf("\t\tThe vocabulary will be saved to <file>\n");
        printf("\t-read-vocab <file>\n");
        printf("\t\tThe vocabulary will be read from <file>, not constructed from the training data\n");
        printf("\t-cbow <int>\n");
        printf("\t\tUse the continuous back of words model; default is 0 (skip-gram model)\n");
        printf("\nExamples:\n");
        printf("./word2vec -train data.txt -output vec.txt -size 200 -window 5 -sample 1e-4 -negative 5 -hs 0 -binary 0 -cbow 1 -iter 3\n\n");
        return 0;
    }
    output_file[0] = 0;
    save_vocab_file[0] = 0;
    read_vocab_file[0] = 0;
    if ((i = ArgPos((char *)"-size", argc, argv)) > 0) layer1_size = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-train", argc, argv)) > 0) strcpy(train_file, argv[i + 1]);
    if ((i = ArgPos((char *)"-save-vocab", argc, argv)) > 0) strcpy(save_vocab_file, argv[i + 1]);
    if ((i = ArgPos((char *)"-read-vocab", argc, argv)) > 0) strcpy(read_vocab_file, argv[i + 1]);
    if ((i = ArgPos((char *)"-debug", argc, argv)) > 0) debug_mode = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-binary", argc, argv)) > 0) binary = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-cbow", argc, argv)) > 0) cbow = atoi(argv[i + 1]);
    if (cbow) alpha = 0.05;
    if ((i = ArgPos((char *)"-alpha", argc, argv)) > 0) alpha = atof(argv[i + 1]);
    if ((i = ArgPos((char *)"-output", argc, argv)) > 0) strcpy(output_file, argv[i + 1]);
    if ((i = ArgPos((char *)"-window", argc, argv)) > 0) window = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-sample", argc, argv)) > 0) sample = atof(argv[i + 1]);
    if ((i = ArgPos((char *)"-hs", argc, argv)) > 0) hs = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-negative", argc, argv)) > 0) negative = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-threads", argc, argv)) > 0) num_threads = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-iter", argc, argv)) > 0) iter = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-min-count", argc, argv)) > 0) min_count = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-classes", argc, argv)) > 0) classes = atoi(argv[i + 1]);
    vocabs = (struct vocab_suf *)calloc(vocabs_max_size, sizeof(struct vocab_suf));               // Allocate initial vocab!
    vocabm = (struct vocab_stem *)calloc(vocabm_max_size, sizeof(struct vocab_stem));               // Allocate initial vocab!
    vocabw = (struct vocab_word *)calloc(vocabw_max_size, sizeof(struct vocab_word));               // Allocate initial list of words
    vocabs_hash = (int *)calloc(vocab_hash_size, sizeof(int));                                      // Allocate hash table
    vocabw_hash = (int *)calloc(vocab_hash_size, sizeof(int));                                      // Allocate hash table
    vocabm_hash = (int *)calloc(vocab_hash_size, sizeof(int));                                      // Allocate hash table
    expTable = (real *)malloc((EXP_TABLE_SIZE + 1) * sizeof(real));                                 // Allocate exponencial table
    for (i = 0; i < EXP_TABLE_SIZE; i++) {
        expTable[i] = exp((i / (real)EXP_TABLE_SIZE * 2 - 1) * MAX_EXP);                            // Pre-compute the exp() table
        expTable[i] = expTable[i] / (expTable[i] + 1);                                              // Pre-compute f(x) = x / (x + 1) -> range(0, 1)
    }
    TrainModel();
    return 0;                                                                                       // Everything done without errors.
}
