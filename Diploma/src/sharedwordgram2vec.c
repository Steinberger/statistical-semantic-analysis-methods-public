//  Copyright 2013 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>

#define MAX_STRING 100                                                  // Maximum length of string
#define EXP_TABLE_SIZE 1000                                             // Maximum size of EXP table
#define MAX_EXP 6                                                       // Maximum parameter of exp!
#define MAX_SENTENCE_LENGTH 1000                                        // Longer sentences are ignored
#define MAX_CODE_LENGTH 40                                              // Max code length of Huffman code

#define LEFT_EDGE '<'
#define RIGHT_EDGE '>'

const int vocab_hash_size = 30000000;                                   // Maximum 30 * 0.7 = 21M words in the vocabulary, apply also for ngrams

// Just rename float to real
typedef float real;                                                     // Precision of float numbers

/**
 Structure representing gram in vocabulary
 */
struct vocab_gram {                                                       // Single gram
    long long cn;                                                         // Count - gram occurrences
    char *gram;                                                           // String representation, Huffman code, Length of Huffman code
};

/**
 * Structure representing word in vocabulary
 */
struct vocab_word {
    long long cn;                                                       // Count - word occurrences
    int *grams, *point;                                                 // Grams indices, sequence of decision nodes (parent indices) in decision tree
    char *word, gramlen, *code, codelen;                                // String representation, number of grams, Huffman code, Length of huffman code
};

char train_file[MAX_STRING], output_file[MAX_STRING];                   // Train/output file paths
char save_vocab_file[MAX_STRING], read_vocab_file[MAX_STRING];          // Save/Load vocabulary paths
struct vocab_gram *vocabg;                                              // Vocabulary of grams
struct vocab_word *vocabw;                                              // Vocabulary of words
int ngrams = 4;                                                         // N-grams option -> 0 - disabled, ngrams > 0 - enabled and also size of grams
int binary = 0;                                                         // Binary representation of results or not
int cbow = 0;                                                           // CBOW or Skip-gram
int debug_mode = 2;                                                     // Level of debug
int window = 5;                                                         // Maximum! one-side! length of window -> randomly choosing
int min_count = 5;                                                      // Min word occurrences threshold
int num_threads = 1;                                                    // Number of parallel threads
int unknown = 1;                                                        // Unknown n-gram switch -> default on
int boundaries = 1;                                                     // N-gram boundaries switch -> default on
int min_reduce = 1;                                                     // Another min word occurrences threshold -> additional reducing
int *vocabg_hash;                                                       // Vocab_hashg[hash(gram)] = index -> vocabg[index] = gram
int *vocabw_hash;                                                       // Vocabw_hash[hash(word)] = index -> vocabw[index] = word
long long vocabg_max_size = 1000;                                       // Current max size of vocab -> dynamically expanding
long long vocabw_max_size = 1000;                                       // Current max size of words -> dynamically expanding
long long vocabg_size = 0;                                              // Actual size of vocab
long long vocabw_size = 0;                                              // Actual size of words
long long layer1_size = 100;                                            // Size (dimension) of all layers in neural network
long long iter = 5;                                                     // Number of iterations
long long train_words = 0;                                              // Number of train words (all words -> including multiple occurrences of same word)
long long train_grams = 0;                                              // Number of train grams
long long word_count_actual = 0;                                        // Already trained words
long long file_size = 0;                                                // Size of input file
long long classes = 0;                                                  // Number of classification classes for K-means
real alpha = 0.025, starting_alpha;                                     // Current and starting alpha -> learning rate
real sample = 1e-3;                                                     // Subsampling value...If sample > 0 -> subsampling on!
real *syn0;                                                             // Input -> Projection weights  - Vocab * layer_size
real *syn1;                                                             // HS: Projection -> Output weights - Vocab * layer_size
real *syn1neg;                                                          // NEG: Projection -> Output weights - Vocab * layer_size
real *expTable;
clock_t start;                                                          // Start time for monitoring

int hs = 0;                                                             // Hiearchical softmax
int negative = 5;                                                       // Negative sampling value -> negative > 0 = sampling on
const int table_size = 1e8;                                             // Distribution table for Negative sampling
int *table;                                                             // Distribution table itself

char *UNKNOWN = "UNKNOWN";                                              // Unknown ngram

/**
 * Print mapping of words to grams. Debug == 3
 */
void printWord2grams() {
    int w, g;
    printf("-------------------\n");
    printf("Printing word2grams\n");
    printf("-------------------\n");
    for(w = 0; w < vocabw_size; w++) {
        for(g = 0; g < vocabw[w].gramlen; g++) {
            printf("%s ", vocabg[vocabw[w].grams[g]].gram);
        }
        printf("\n");
    }
    printf("-------------------\n");
    printf("End of word2grams\n");
    printf("-------------------\n");
}

/**
 * Strlen supporting UTF-8...Only 2 byte characters!
 */
int utf8_strlen(char *word) {
    int i = 0, j = 0;
    while (word[i++]) if ((word[i] & 0xC0) != 0x80) j++;                // Counting only first bytes
    return j;
}

/**
 * Strncpy supporting UTF-8...Only 2 byte characters!
 */
int utf8_strncpy(char *dest, char *src, int from, int length) {
    int i = 0, j = 0;
    while(src[j] && from > 0) {                                         // Find true start position
        from--;j++;                                                     // Go on next position and decrement starting index
        if((src[j] & 0xC0) == 0x80) j++;                                // Pointer on second byte of utf-8 -> continue
    }
    while(src[j] && length > 0) {                                       // Copy whole chars
        if((src[j] & 0xC0) != 0x80) length--;
        dest[i++] = src[j++];
    }
    if(src[j] && (src[j] & 0xC0) == 0x80) dest[i++] = src[j++];         // Last char is two bytes
    dest[i] = '\0';                                                     // Ending char
    return i - 1;                                                       // Number of BYTES copied
}

/**
 Create unigram table from words

 Only if Negative sampling!!! Created from sorted table.
 */
void InitUnigramTable() {
    int a, i;
    double train_words_pow = 0;                                                       // Summary of powered values
    double d1, power = 0.75;                                                          // Power constant
    table = (int *)malloc(table_size * sizeof(int));                                  // Allocate table
    for (a = 0; a < vocabw_size; a++) train_words_pow += pow(vocabw[a].cn, power);    // Some summary of powers of counts
    i = 0;                                                                            // Start at the beginning!
    d1 = pow(vocabw[i].cn, power) / train_words_pow;                                  // How many percent of all words
    for (a = 0; a < table_size; a++) {                                                // Loop over the table
        table[a] = i;                                                                   // Current word
        if (a / (double)table_size > d1) {                                              // If percentage bigger -> go for next word
            i++;                                                                          // Next word
            d1 += pow(vocabw[i].cn, power) / train_words_pow;                             // Next percentage
        }
        if (i >= vocabw_size) i = vocabw_size - 1;                                      // No more words -> use last word
    }
}

/**
 Read word. End of line -> </s>

 Native comment:
 Reads a single word from a file, assuming space + tab + EOL to be word boundaries
 */
void ReadWord(char *word, FILE *fin) {
    int a = 0, ch;
    while (!feof(fin)) {
        ch = fgetc(fin);                                            // Read char
        if (ch == 13) continue;                                     // CR - continue
        if ((ch == ' ') || (ch == '\t') || (ch == '\n')) {
            if (a > 0) {
                if (ch == '\n') ungetc(ch, fin);                    // Return back \n
                break;                                              // Stop adding
            }
            if (ch == '\n') {                                       // End of line
                strcpy(word, (char *)"</s>");                       // End of line = </s>
                return;
            } else continue;                                        // Strip whitespaces
        }
        word[a] = ch;                                               // Add next character
        a++;
        if (a >= MAX_STRING - 1) a--;                               // Truncate too long words
    }
    word[a] = 0;                                                    // Add end of string \0
}

/**
 Word->hashCode()


 Native comment:
 Returns hash value of a word
 */
int StringHash(char *string) {
    unsigned long long a, hash = 0;
    for (a = 0; a < strlen(string); a++) hash = hash * 257 + string[a];       // Hash function
    hash = hash % vocab_hash_size;                                            // Fit vocab size
    return hash;                                                              // Return hash
}

/**
 Index of gram in vocabulary.

 Native comment:
 Returns position of a gram in the vocabulary; if the gram is not found, returns -1
 */
int SearchVocabg(char *gram) {
    unsigned int hash = StringHash(gram);                                // Get hash code
    while (1) {
        if (vocabg_hash[hash] == -1) return -1;                          // Word not found -> -1
        if (!strcmp(gram, vocabg[vocabg_hash[hash]].gram)) return vocabg_hash[hash];
        hash = (hash + 1) % vocab_hash_size;                             // Hash collision -> hash + 1
    }
    return -1;                                                           // Word not found -> -1
}

/**
 Index of word in vocabulary.
 Returns position of a word in the vocabulary; if the word is not found, returns -1
 */
int SearchVocabw(char *word) {
    unsigned int hash = StringHash(word);                                       // Get hash code
    while (1) {
        if (vocabw_hash[hash] == -1) return -1;                                 // Word not found -> -1
        if (!strcmp(word, vocabw[vocabw_hash[hash]].word)) return vocabw_hash[hash];
        hash = (hash + 1) % vocab_hash_size;                                    // Hash collision -> hash + 1
    }
    return -1;                                                                  // Word not found -> -1
}

/**
 Reads a word and returns its index in the vocabulary
 */
int ReadWordIndex(FILE *fin) {
    char word[MAX_STRING];
    ReadWord(word, fin);
    if (feof(fin)) return -1;
    return SearchVocabw(word);
}

/*
 * Reduces the vocabulary by removing infrequent tokens.
 *
 * After reduce must be called sort to initialize huffman codes!!
 */
void ReduceVocabg() {
    int a, b = 1;                                         // b=1 -> Skip UNKNOWN n-gram
    unsigned int hash;
    for (a = 1; a < vocabg_size; a++)                     // a=1 -> Skip UNKNOWN n-gram
        if (vocabg[a].cn > min_reduce) {                  // Reducing threshold
            vocabg[b].cn = vocabg[a].cn;                                                // Just shifting to the left!
            vocabg[b].gram = vocabg[a].gram;
            b++;
        } else free(vocabg[a].gram);                                                    // Free if smaller
    vocabg_size = b;                                                                    // Actual size of vocab
    for (a = 0; a < vocab_hash_size; a++) vocabg_hash[a] = -1;                          // Clear hash table
    for (a = 0; a < vocabg_size; a++) {                                                 // Recompute hash table
        // Hash will be re-computed, as it is not actual
        hash = StringHash(vocabg[a].gram);                                              // Word->hashCode()
        while (vocabg_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;            // Hash collision -> hash + 1
        vocabg_hash[hash] = a;                                                          // vocab_hash[word->hashCode()] = a -> vocab[a] = word
    }
    fflush(stdout);                                                                     // Flush debug messages
    min_reduce++;                                                                       // Increment threshold
}

/*
 * Reduces the vocabulary by removing infrequent tokens.
 *
 * After reduce must be called sort to initialize ngrams!!
 */
void ReduceVocabw() {
    int a, b = 1;                                           // b=1 -> Skip <s/> word
    unsigned int hash;
    for (a = 1; a < vocabw_size; a++)                       // a=1 -> Skip <s/> word
        if (vocabw[a].cn > min_reduce) {                    // Reducing threshold
            vocabw[b].cn = vocabw[a].cn;                                              // Just shifting to the left!
            vocabw[b].word = vocabw[a].word;
            b++;
        } else free(vocabw[a].word);                                                  // Free if smaller
    vocabw_size = b;                                                                  // Actual size of vocab
    for (a = 0; a < vocab_hash_size; a++) vocabw_hash[a] = -1;                        // Clear hash table
    for (a = 0; a < vocabw_size; a++) {                                               // Recompute hash table
        // Hash will be re-computed, as it is not actual
        hash = StringHash(vocabw[a].word);                                            // Word->hashCode()
        while (vocabw_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;          // Hash collision -> hash + 1
        vocabw_hash[hash] = a;                                                        // vocab_hash[word->hashCode()] = a -> vocab[a] = word
    }
    fflush(stdout);                                                                   // Flush debug messages
    min_reduce++;                                                                     // Increment threshold
}

// Adds a gram to the vocabulary of grams
int AddGramToVocab(char *word, int start, int end, int count) {
    unsigned int hash, pos, length = end - start;                               // +1 for \0
    if (length > MAX_STRING) length = MAX_STRING;                               // Maximum length
    char gram[MAX_STRING];
    utf8_strncpy(gram, word, start, length);
    train_grams++;
    pos = SearchVocabg(gram);
    if(pos == -1) {
        vocabg[vocabg_size].gram = (char *)calloc(length, sizeof(char));        // Allocate space for string
        strcpy(vocabg[vocabg_size].gram, gram);                                 // Copy string -> characters
        vocabg[vocabg_size].cn = count;                                         // Number of occurrences
        pos = vocabg_size;
        vocabg_size++;                                                          // Next position in vocab
        // Reallocate memory if needed
        if (vocabg_size + 2 >= vocabg_max_size) {                               // Vocab is small -> reallocate
            vocabg_max_size += 1000;                                            // Growing constant
            vocabg = (struct vocab_gram *)realloc(vocabg, vocabg_max_size * sizeof(struct vocab_gram));  // Reallocation
        }
        hash = StringHash(gram);                                                // Get hash code of word
        while (vocabg_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;    // Hash collision -> hash + 1
        vocabg_hash[hash] = vocabg_size - 1;                                    // Save vocab index into vocab_hash on hash position
    } else {
        vocabg[pos].cn += count;
    }
    return pos;
}

/**
 Add a word to the vocabulary of words
 */
int AddWordToVocab(char *word) {
    unsigned int hash, length = strlen(word) + 1;
    if (length > MAX_STRING) length = MAX_STRING;
    vocabw[vocabw_size].word = (char *)calloc(length, sizeof(char));
    strcpy(vocabw[vocabw_size].word, word);
    vocabw[vocabw_size].cn = 0;
    vocabw_size++;
    // Reallocate memory if needed
    if (vocabw_size + 2 >= vocabw_max_size) {
        vocabw_max_size += 1000;
        vocabw = (struct vocab_word *)realloc(vocabw, vocabw_max_size * sizeof(struct vocab_word));
    }
    hash = StringHash(word);
    while (vocabw_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
    vocabw_hash[hash] = vocabw_size - 1;
    return vocabw_size - 1;
}

// Used later for sorting by word counts --> Descending!!!
int VocabgCompare(const void *a, const void *b) {
    return ((struct vocab_gram *)b)->cn - ((struct vocab_gram *)a)->cn;
}

// Used later for sorting by word counts --> Descending!!!
int VocabwCompare(const void *a, const void *b) {
    return ((struct vocab_word *)b)->cn - ((struct vocab_word *)a)->cn;
}

// Sorts the vocabulary by frequency using word counts
void SortVocabg() {
    int a, size;
    unsigned int hash;
    // Sort the vocabulary and keep <UNKNOWN> at the first position
    qsort(&vocabg[1], vocabg_size - 1, sizeof(struct vocab_gram), VocabgCompare);        // Quick sort on vocab according to occurences
    for (a = 0; a < vocab_hash_size; a++) vocabg_hash[a] = -1;                           // Clear indices
    vocabg_hash[StringHash(vocabg[0].gram)] = 0;
    size = vocabg_size;                                                                  // Vocab actual size
    train_grams = 0;                                                                     // Train words initial value -> 0
    for (a = 1; a < size; a++) { // Skip </s>                                            // a = 1 -> skip </s>
        // Words occuring less than min_count times will be discarded from the vocab
        if (vocabg[a].cn < min_count) {                                                  // Min count threshold
            vocabg_size--;                                                               // Vocabulary is smaller
            free(vocabg[a].gram);                                                        // Free memory of string -> characters
        } else {
            // Hash will be re-computed, as after the sorting it is not actual
            hash=StringHash(vocabg[a].gram);                                             // Hashcode of word
            while (vocabg_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;         // Collision -> hash + 1
            vocabg_hash[hash] = a;                                                       // a = index in vocab -> vocab_hash[hash(word)] = a -> vocab[a] = word
            train_grams += vocabg[a].cn;                                                 // Add occurrences count to train_words summary
        }
    }
    vocabg = (struct vocab_gram *)realloc(vocabg, (vocabg_size + 1) * sizeof(struct vocab_gram));    // Reallocate memory to fit exactly the size of vocabulary!
}

/**
 * Sort word vocabulary according to occurrences counts.
 *
 * SortVocabg must be called in the first place!!!
 */
void SortVocabw() {
    int a, size, length, gram, i, b = 1;
    char string[ngrams + 1];
    unsigned int hash;
    // Sort the vocabulary and keep </s> at the first position
    qsort(&vocabw[1], vocabw_size - 1, sizeof(struct vocab_word), VocabwCompare);               // Quick sort on vocab according to occurences
    for (a = 0; a < vocab_hash_size; a++) vocabw_hash[a] = -1;                                  // Clear indices
    vocabw_hash[StringHash(vocabw[0].word)] = 0;
    size = vocabw_size;                                                                         // Vocab actual size
    train_words = 0;                                                                            // Train words initial value -> 0
    for (a = 1; a < size; a++) { // Skip </s>                                                   // a = 1 -> skip </s>
        vocabw[b].word = vocabw[a].word;                                                        // Move current word to true position
        vocabw[b].cn = vocabw[a].cn;
        if(vocabw[b].cn >= min_count) {                                                         // Good sample -> enough occurrences
            length = utf8_strlen(vocabw[b].word);                                               // size of sample
            if(length < ngrams) {                                                               // size smaller than ngram size -> use whole string
                gram = SearchVocabg(vocabw[b].word);                                            // Search ngram index
                if(gram == -1 && !unknown) {                                                    // Not found and UNKNOWN ngram turned off
                    vocabw[b].cn = 0;                                                           // -> remove word anyway
                } else {
                    if(gram == -1) gram = SearchVocabg(UNKNOWN);                                // Not found search for UNKNOWN gram
                    vocabw[b].gramlen = 1;                                                      // Only one gram
                    vocabw[b].grams = (int *) calloc(1, sizeof(int));                           // Allocate memory for ngram list
                    vocabw[b].grams[0] = gram;                                                  // Safe the one ngram
                }
            } else {                                                                            // Iterate over all ngrams -> at least 2
                if(!boundaries) {
                    vocabw[b].gramlen = length - ngrams + 1;                                        // Number of ngrams
                    vocabw[b].grams = (int *) calloc(vocabw[b].gramlen, sizeof(int));               // Allocate array for ngrams
                    for(i = 0; i < vocabw[a].gramlen; i++) {                                        // Fill the array
                        utf8_strncpy(string, vocabw[b].word, i, ngrams);                            // Create ngram
                        gram = SearchVocabg(string);                                                // Find index
                        if(gram == -1 && !unknown) {                                                // In case of UNKNOWN ngram turned off
                            vocabw[b].cn = 0;                                                       // -> Remove the word anyway
                            vocabw[b].gramlen = 0;
                            free(vocabw[b].grams);
                            break;
                        } else {
                            if(gram == -1) gram = SearchVocabg(UNKNOWN);                            // Find index of UNKNOWN gram
                            vocabw[b].grams[i] = gram;                                              // Fill it
                        }
                    }
                } else {
                    char word[MAX_STRING];
                    length = strlen(vocabw[b].word);
                    word[0] = LEFT_EDGE;
                    strncpy(word + 1, vocabw[b].word, length);
                    word[length + 1] = RIGHT_EDGE;
                    word[length + 2] = '\0';
                    length = utf8_strlen(word);
                    vocabw[b].gramlen = length - ngrams + 1;                                        // Number of ngrams
                    vocabw[b].grams = (int *) calloc(vocabw[b].gramlen, sizeof(int));               // Allocate array for ngrams
                    for(i = 0; i < vocabw[b].gramlen; i++) {                                        // Fill the array
                        utf8_strncpy(string, word, i, ngrams);                                      // Create ngram
                        gram = SearchVocabg(string);                                                // Find index
                        if(gram == -1 && !unknown) {                                                // In case of UNKNOWN ngram turned off
                            vocabw[b].cn = 0;                                                       // -> Remove the word anyway
                            vocabw[b].gramlen = 0;
                            free(vocabw[b].grams);
                            break;
                        } else {
                            if(gram == -1) gram = SearchVocabg(UNKNOWN);                            // Find index of UNKNOWN gram
                            vocabw[b].grams[i] = gram;                                              // Fill it
                        }
                    }
                }

            }
        }
        // Words occurring less than min_count times will be discarded from the vocab
        if (vocabw[b].cn < min_count) {                                                    // Min count threshold
            free(vocabw[b].word);                                                          // Free memory of string -> characters
        } else {
            // Hash will be re-computed, as after the sorting it is not actual
            hash=StringHash(vocabw[b].word);                                               // Hashcode of word
            while (vocabw_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;           // Collision -> hash + 1
            vocabw_hash[hash] = b;                                                         // a = index in vocab -> vocab_hash[hash(word)] = a -> vocab[a] = word
            train_words += vocabw[b].cn;                                                   // Add occurrences count to train_words summary
            b++;                                                                           // Current word is OK -> let's go for another
        }
    }
    vocabw_size = b;                                                                                 // Last valid word
    vocabw = (struct vocab_word *)realloc(vocabw, (vocabw_size + 1) * sizeof(struct vocab_word));    // Reallocate memory to fit exactly the size of vocabulary!
    for (a = 0; a < vocabw_size; a++) {
        vocabw[a].code = (char *)calloc(MAX_CODE_LENGTH, sizeof(char));                  // Alloc space for Huffman code
        vocabw[a].point = (int *)calloc(MAX_CODE_LENGTH, sizeof(int));                   // Alloc space for Parents
    }
}

/**
 Creates Huffman tree!



 // Create binary Huffman tree using the word counts
 // Frequent words will have short uniqe binary codes
 */
void CreateBinaryTree() {
    long long a, b, i, min1i, min2i, pos1, pos2, point[MAX_CODE_LENGTH];                       // Help variables
    char code[MAX_CODE_LENGTH];                                                                // Current code
    long long *count = (long long *)calloc(vocabw_size * 2 + 1, sizeof(long long));            // Word->cn quick reference
    long long *binary = (long long *)calloc(vocabw_size * 2 + 1, sizeof(long long));           // 1 or 0
    long long *parent_node = (long long *)calloc(vocabw_size * 2 + 1, sizeof(long long));      // Parent indices
    for (a = 0; a < vocabw_size; a++) count[a] = vocabw[a].cn;                                 // Copy occurrence counts
    for (a = vocabw_size; a < vocabw_size * 2; a++) count[a] = 1e15;                           // Fill rest with big numbers -> We will look for minimum
    pos1 = vocabw_size - 1;                                                                    // In the middle - 1
    pos2 = vocabw_size;                                                                        // In the middle
    // Following algorithm constructs the Huffman tree by adding one node at a time
    for (a = 0; a < vocabw_size - 1; a++) {                                                    // Go through vocabulary
        // First, find two smallest nodes 'min1, min2'
        if (pos1 >= 0) {                                          // |<-- left edge
            if (count[pos1] < count[pos2]) {                      // left < right
                min1i = pos1;                                     // Left min = left index
                pos1--;                                           // <-- go left <-- left index
            } else {                                              // left > right
                min1i = pos2;                                     // Left min = right index
                pos2++;                                           // --> go right --> right index
            }
        } else {                                                  // What to do on the edge
            min1i = pos2;                                         // Left min = right index
            pos2++;                                               // --> go right --> right index
        }
        if (pos1 >= 0) {                                          // Just looking for second min
            if (count[pos1] < count[pos2]) {
                min2i = pos1;
                pos1--;
            } else {
                min2i = pos2;
                pos2++;
            }
        } else {
            min2i = pos2;
            pos2++;
        }
        count[vocabw_size + a] = count[min1i] + count[min2i];    // Filling empty counts on the right side -> filled with summary of two least used words
        parent_node[min1i] = vocabw_size + a;                    // Going up In the tree
        parent_node[min2i] = vocabw_size + a;                    // Parent is same for both children
        binary[min2i] = 1;                                       // Going right -> 1, Going left -> 0 (Prefilled with zeros)
    }

    // Now assign binary code to each vocabulary word
    for (a = 0; a < vocabw_size; a++) {                             // a -> Current word in vocab
        b = a;                                                      // b -> Current node in tree
        i = 0;                                                      // start of codes and points
        while (1) {                                                 // Infinite loop
            code[i] = binary[b];                                    // Add code mark according to current node (parent)
            point[i] = b;                                           // Decision node in tree (index)
            i++;                                                    // Next code / point
            b = parent_node[b];                                     // Go to parent
            if (b == vocabw_size * 2 - 2) break;                    // Root reached!
        }
        vocabw[a].codelen = i;                                      // Codelength = number of parents in tree = depth of node
        vocabw[a].point[0] = vocabw_size - 2;                       // Point[0] = root -> also removed vocab_size offset
        for (b = 0; b < i; b++) {                                   // Reverse filling of array
            vocabw[a].code[i - b - 1] = code[b];                    // Assign code to word
            vocabw[a].point[i - b] = point[b] - vocabw_size;        // Just remove vocab_size offset
        }
    }
    free(count);              // Free temp references
    free(binary);
    free(parent_node);
}

/**
 Create vocabulary from input file.
 */
void LearnVocabFromTrainFile() {
    char word[MAX_STRING];                                             // Space for actual word
    FILE *fin;                                                         // Input file
    int i, length;
    long long a;
    for (a = 0; a < vocab_hash_size; a++) vocabg_hash[a] = -1;         // Clear hash table
    for (a = 0; a < vocab_hash_size; a++) vocabw_hash[a] = -1;         // Clear hash table
    fin = fopen(train_file, "rb");                                     // Input file = train file
    if (fin == NULL) {                                                 // Not found
        printf("ERROR: training data file not found!\n");
        exit(1);
    }
    vocabw_size = vocabg_size = 0;                                      // Actual vocabulary size
    AddWordToVocab((char *)"</s>");                                     // Add </s> \n char to vocabulary
    AddGramToVocab(UNKNOWN, 0, 7, 1000000);                             // Add UNKNOWN ngram to vocabulary - 0, 7 size of the gram
    while (1) {                                                         // Just loop
        ReadWord(word, fin);                                            // word = next word
        if (feof(fin)) break;                                           // Just stop looping
        train_words++;
        if ((debug_mode > 1) && (train_words % 100000 == 0)) {          // Debug print
            printf("%lldK%c", train_words / 1000, 13);
            fflush(stdout);
        }
        i = SearchVocabw(word);
        if (i == -1) {
            a = AddWordToVocab(word);
            vocabw[a].cn = 1;
        } else vocabw[i].cn++;
        if (vocabw_size > vocab_hash_size * 0.7) ReduceVocabw();
        length = utf8_strlen(word);
        if(length < ngrams) AddGramToVocab(word, 0, length, 1);
        else {
            for(i = 0; i + ngrams <= length; i++) AddGramToVocab(word, i, i + ngrams, 1);
            if(boundaries) {                                                // Add boundaries
                length = strlen(word);                                      // Size of string
                word[length] = RIGHT_EDGE;                                  // Add >
                word[length + 1] = '\0';                                    // New end of string
                AddGramToVocab(word, i, i + ngrams, 1);                     // Add last ngram
                for(i = length + 1; i >= 0; i--) word[i + 1] = word[i];     // Shift bytes to the right
                word[0] = LEFT_EDGE;                                        // Add <
                AddGramToVocab(word, 0, ngrams, 1);                         // Add first ngram
            }
        }
        if (vocabg_size > vocab_hash_size * 0.7) ReduceVocabg();
    }
    SortVocabg();
    SortVocabw();                                                      // Sort the vocab
    if (debug_mode > 2) printWord2grams();
    if (debug_mode > 0) {                                              // Debug print
        printf("Word Vocab size: %lld\n", vocabw_size);
        printf("Words in train file: %lld\n", train_words);
        printf("Ngram Vocab size: %lld\n", vocabg_size);
        printf("Ngrams in train file: %lld\n", train_grams);
    }
    file_size = ftell(fin);                                           // Save file size
    fclose(fin);                                                      // Close stream
}

/**
 Save created vocabulary in file. Text representation
 */
void SaveVocab() {
    long long i;
    FILE *fo = fopen(save_vocab_file, "wb");
    for (i = 0; i < vocabw_size; i++) fprintf(fo, "%s %lld\n", vocabw[i].word, vocabw[i].cn);
    fclose(fo);
}

/**
 Read vocabulary from file.
 */
void ReadVocab() {
    long long a, i = 0;
    char c;
    char word[MAX_STRING];
    int length;
    FILE *fin = fopen(read_vocab_file, "rb");
    if (fin == NULL) {
        printf("Vocabulary file not found\n");
        exit(1);
    }
    for (a = 0; a < vocab_hash_size; a++) vocabg_hash[a] = -1;         // Clear hash table
    for (a = 0; a < vocab_hash_size; a++) vocabw_hash[a] = -1;         // Clear hash table
    vocabw_size = 0;
    vocabg_size = 0;
    while (1) {
        ReadWord(word, fin);
        if (feof(fin)) break;
        a = AddWordToVocab(word);
        fscanf(fin, "%lld%c", &vocabw[a].cn, &c);
        length = utf8_strlen(word);
        if(length < ngrams) AddGramToVocab(word, 0, length, vocabw[a].cn);
        else {
            for(i = 0; i + ngrams <= length; i++) AddGramToVocab(word, i, i + ngrams, vocabw[a].cn);
            if(boundaries) {                                                // Add boundaries
                length = strlen(word);                                      // Size of string
                word[length] = RIGHT_EDGE;                                  // Add >
                word[length + 1] = '\0';                                    // New end of string
                AddGramToVocab(word, i, i + ngrams, vocabw[a].cn);          // Add last ngram
                for(i = length + 1; i >= 0; i--) word[i + 1] = word[i];     // Shift bytes to the right
                word[0] = LEFT_EDGE;                                        // Add <
                AddGramToVocab(word, 0, ngrams, vocabw[a].cn);              // Add first ngram
            }
        }
        if (vocabg_size > vocab_hash_size * 0.7) ReduceVocabg();
    }
    SortVocabg();
    SortVocabw();   // Re-sort vocabulary
    if (debug_mode > 2) printWord2grams();
    if (debug_mode > 0) {                                              // Debug print
        printf("Word Vocab size: %lld\n", vocabw_size);
        printf("Words in train file: %lld\n", train_words);
        printf("Ngram Vocab size: %lld\n", vocabg_size);
        printf("Ngrams in train file: %lld\n", train_grams);
    }
    fin = fopen(train_file, "rb");
    if (fin == NULL) {
        printf("ERROR: training data file not found!\n");
        exit(1);
    }
    fseek(fin, 0, SEEK_END);
    file_size = ftell(fin);
    fclose(fin);
}

/**
 Initialize neural network.
 */
void InitNet() {
    long long a, b;
    unsigned long long next_random = 1;
    a = posix_memalign((void **)&syn0, 128, (long long) (vocabg_size + vocabw_size) * layer1_size * sizeof(real)); // Input -> Projection weights - vocab_size x layer1_size
    if (syn0 == NULL) {printf("Memory allocation failed\n"); exit(1);}
    if (hs) {                                                                                                      // HS: Projection -> Output weights - vocab_size x layer1_size
        a = posix_memalign((void **)&syn1, 128, (long long)vocabw_size * layer1_size * sizeof(real));
        if (syn1 == NULL) {printf("Memory allocation failed\n"); exit(1);}
        for (a = 0; a < vocabw_size; a++) for (b = 0; b < layer1_size; b++) syn1[a * layer1_size + b] = 0;         // Filled with zeros
    }
    if (negative>0) {                                                                                              // NEG: Projection -> Output weights - vocab_size x layer1_size
        a = posix_memalign((void **)&syn1neg, 128, (long long)vocabw_size * layer1_size * sizeof(real));
        if (syn1neg == NULL) {printf("Memory allocation failed\n"); exit(1);}
        for (a = 0; a < vocabw_size; a++) for (b = 0; b < layer1_size; b++) syn1neg[a * layer1_size + b] = 0;      // Filled with zeros
    }
    for (a = 0; a < (vocabg_size + vocabw_size); a++) for (b = 0; b < layer1_size; b++) {
            next_random = next_random * (unsigned long long)25214903917 + 11;
            syn0[a * layer1_size + b] = (((next_random & 0xFFFF) / (real)65536) - 0.5) / layer1_size;              // Fill initial (Input -> Projection) weights -> (random number < 0.5) / layer_size
        }
    CreateBinaryTree();                                                                                            // Create the Huffman tree
}

/**
 Train neural network! Main method.

 Thread runnable method.
 */
void *TrainModelThread(void *id) {
    long long a, b, d, cw, word, last_word, sentence_length = 0, sentence_position = 0; // Helpers
    long long word_count = 0, last_word_count = 0, sen[MAX_SENTENCE_LENGTH + 1];        // Helpers
    long long l2, c, target, label, local_iter = iter;                                  // Helpers
    int i;
    unsigned long long next_random = (long long)id;                                     // Random generator seed -> id of thread
    real f;                                                                             // Output value
    real g;                                                                             // Gradient value
    clock_t now;                                                                        // Time helper
    real *neu1 = (real *)calloc(layer1_size, sizeof(real));                             // Projection layer
    real *neu1e = (real *)calloc(layer1_size, sizeof(real));                            // Projection layer errors
    long long goff = vocabw_size * layer1_size;
    FILE *fi = fopen(train_file, "rb");                                                 // Open train file
    fseek(fi, file_size / (long long)num_threads * (long long)id, SEEK_SET);            // Load assigned data
    while (1) {                                                                         // While not EOF
        if (word_count - last_word_count > 10000) {                                     // Just printing information
            word_count_actual += word_count - last_word_count;                          // Get number for this step
            last_word_count = word_count;                                               // Renew last word count
            if ((debug_mode > 1)) {                                                     // If debug on -> print
                now=clock();                                                            // Save time
                printf("%cAlpha: %f  Progress: %.2f%%  Words/thread/sec: %.2fk  ", 13, alpha, word_count_actual / (real)(iter * train_words + 1) * 100, word_count_actual / ((real)(now - start + 1) / (real)CLOCKS_PER_SEC * 1000));
                fflush(stdout);                                                         // Print statistics
            }
            alpha = starting_alpha * (1 - word_count_actual / (real)(iter * train_words + 1));   // Make smaller learning alpha
            if (alpha < starting_alpha * 0.0001) alpha = starting_alpha * 0.0001;         // If alfa is too small
        }
        if (sentence_length == 0) {                                                       // Sentence not loaded
            while (1) {
                word = ReadWordIndex(fi);                                                 // Read word from line
                if (feof(fi)) break;                                                      // EOF -> End of main loop
                if (word == -1) continue;                                                 // Unknown word
                word_count++;
                if (word == 0) break;                                                     // 0 -> <s> -> End of line -> Sentence loaded
                // The subsampling randomly discards frequent words while keeping the ranking same
                if (sample > 0) {                                                         // Subsampling on!
                    real ran = (sqrt(vocabw[word].cn / (sample * train_words)) + 1) * (sample * train_words) / vocabw[word].cn;     // Generate threshold value
                    next_random = next_random * (unsigned long long)25214903917 + 11;                                               // Generate next random value
                    if (ran < (next_random & 0xFFFF) / (real)65536) continue;                                                       // Compare generated threshold with random value
                }
                sen[sentence_length] = word;                                              // Save word into sentence
                sentence_length++;                                                        // Increment sentence length
                if (sentence_length >= MAX_SENTENCE_LENGTH) break;                        // Cut too long sentences -> there is a limit
            }
            sentence_position = 0;                                                        // Starting with first word
        }
        if (feof(fi) || (word_count > train_words / num_threads)) {
            word_count_actual += word_count - last_word_count;
            local_iter--;
            if (local_iter == 0) break;
            word_count = 0;
            last_word_count = 0;
            sentence_length = 0;
            fseek(fi, file_size / (long long) num_threads * (long long) id, SEEK_SET);
            continue;
        }
        word = sen[sentence_position];                                              // Get current word
        if (word == -1) continue;                                                   // Unknown word -> probably just check (unknown words should be already skipped)
        for (c = 0; c < layer1_size; c++) neu1[c] = 0;                              // Projection layer is cleared for every word
        for (c = 0; c < layer1_size; c++) neu1e[c] = 0;                             // Errors for Projection layer are cleared for every word
        next_random = next_random * (unsigned long long)25214903917 + 11;           // Generate next random value
        b = next_random % window;                                                   // Set random size of window
        if (cbow) {  //train the cbow architecture
            // Words! in -> Projection
            cw = 0;
            for (a = b; a < window * 2 + 1 - b; a++) {
                if (a != window) {                                                  // Clever loop for iterating over random size window!
                    c = sentence_position - window + a;
                    if (c < 0) continue;
                    if (c >= sentence_length) continue;
                    last_word = sen[c];                                                     // Current word in window in sentence
                    if (last_word == -1) continue;                                          // Not in dictionary
                    for(c = 0; c < layer1_size; c++) neu1[c] += syn0[c + last_word * layer1_size];
                    for (i = 0; i < vocabw[last_word].gramlen; i++) {
                        for (c = 0; c < layer1_size; c++) neu1[c] += syn0[c + vocabw[last_word].grams[i] * layer1_size + goff];
                        cw++;
                    }
                    cw++;
                }
            }
            if(cw) {
                for(c = 0; c < layer1_size; c++) neu1[c] /= cw;
                if (hs) {
                    for (i = 0; i < vocabw[word].codelen; i++) {
                        f = 0;                                                                  // Output value set to 0
                        l2 = vocabw[word].point[i] * layer1_size;                          // Find decision node row = l2
                        // Propagate Projection -> output
                        for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1[c + l2];          // Projection * Projection-output weight
                        if (f <= -MAX_EXP) continue;                                            // If smaller than minumum go for next code
                        else if (f >= MAX_EXP) continue;                                        // If bigger than maximum go for next code
                        else f = expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];   // Just fit f to range (0, 2*MAX_EXP)
                        // 'g' is the gradient multiplied by the learning rate
                        g = (1 - vocabw[word].code[i] - f) * alpha;                              // Gradient = (1|0 - f * alpha) -> 1 -> -f && 0 -> +f
                        // Propagate errors output -> Projection
                        for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1[c + l2];         // Update errors
                        // Learn weights Projection -> output
                        for (c = 0; c < layer1_size; c++) syn1[c + l2] += g * neu1[c];          // Update weights
                    }
                }
                // NEGATIVE SAMPLING
                if (negative > 0)
                    for (d = 0; d < negative + 1; d++) {                    // Iterate over number of negative sampling
                        if (d == 0) {                                                             // D == 0 -> Current word
                            target = word;                                                        // Target is current word
                            label = 1;                                                            // Label = 1 -> Current word
                        } else {
                            next_random = next_random * (unsigned long long) 25214903917 + 11;     // Generate next random value
                            target = table[(next_random >> 16) % table_size];                     // Get random value from table
                            if (target == 0) target = next_random % (vocabw_size - 1) + 1;         // If target == 0 -> Get random word from vocabulary
                            if (target == word) continue;                                         // If target == current word -> Continue
                            label = 0;                                                            // Label = 0 -> Not current word
                        }
                        l2 = target * layer1_size;                             // Layer of target word
                        f = 0;                                                                  // Output value set to 0
                        for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1neg[c + l2];       // Projection * Projection-output weight
                        if (f > MAX_EXP) g = (label - 1) * alpha;                               // If bigger -> fixed gradient -> label 1: 0, label 0: -alpha
                        else if (f < -MAX_EXP) g = (label - 0) * alpha;                         // If smaller -> fixed gradient -> label 1: alpha, label 0: 0
                        else g = (label - expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;   // Gradient = (1|0 - f * alpha) -> 1 -> -f && 0 -> +f
                        for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1neg[c + l2];      // Update errors
                        for (c = 0; c < layer1_size; c++) syn1neg[c + l2] += g * neu1[c];       // Update weights
                    }
                // Projection -> in
                for (a = b; a < window * 2 + 1 - b; a++)
                    if (a != window) {                          // Clever loop for iterating over random size window! Size is same as for in -> Projection process!!!
                        c = sentence_position - window + a;
                        if (c < 0) continue;
                        if (c >= sentence_length) continue;
                        last_word = sen[c];
                        if (last_word == -1) continue;
                        for (c = 0; c < layer1_size; c++) syn0[c + last_word * layer1_size] += neu1e[c];
                        for (i = 0; i < vocabw[last_word].gramlen; i++) {
                            for (c = 0; c < layer1_size; c++) syn0[c + vocabw[last_word].grams[i] * layer1_size + goff] += neu1e[c];    // Add Projection layer errors to input -> Projection weights!
                        }
                    }
            }
        } else {  //train skip-gram
            for (a = b; a < window * 2 + 1 - b; a++)
                if (a != window) {                              // Clever loop for iterating over random size window!
                    c = sentence_position - window + a;
                    if (c < 0) continue;
                    if (c >= sentence_length) continue;
                    last_word = sen[c];
                    if (last_word == -1) continue;

                    // Word
                    for (c = 0; c < layer1_size; c++) neu1[c] = 0;                                  // Clear projection layer -> Each word has its own projection
                    for (c = 0; c < layer1_size; c++) neu1e[c] = 0;                                 // Clear projection layer errors for each word
                    for (c = 0; c < layer1_size; c++) neu1[c] += syn0[c + last_word * layer1_size];
                    for (i = 0; i < vocabw[last_word].gramlen; i++) {                               // Project ngrams
                        for (c = 0; c < layer1_size; c++) neu1[c] += syn0[c + vocabw[last_word].grams[i] * layer1_size + goff];
                    }
                    // HIERARCHICAL SOFTMAX
                    if (hs) {
                        for (d = 0; d < vocabw[word].codelen; d++) {
                            f = 0;                                                              // Output value set to 0
                            l2 = vocabw[word].point[d] * layer1_size;                      // Current code mark row  -> offset
                            // Propagate Projection -> output
                            for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1[c + l2]; // Output value
                            if (f <= -MAX_EXP) continue;
                            else if (f >= MAX_EXP) continue;
                            else f = expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];
                            // 'g' is the gradient multiplied by the learning rate
                            g = (1 - vocabw[last_word].code[d] - f) * alpha;
                            // Propagate errors output -> Projection
                            for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1[c + l2];
                            // Learn weights Projection -> output
                            for (c = 0; c < layer1_size; c++) syn1[c + l2] += g * neu1[c];
                        }
                    }
                    // NEGATIVE SAMPLING
                    if (negative > 0)
                        for (d = 0; d < negative + 1; d++) {
                            if (d == 0) {
                                target = word;
                                label = 1;
                            } else {
                                next_random = next_random * (unsigned long long) 25214903917 + 11;
                                target = table[(next_random >> 16) % table_size];
                                if (target == 0) target = next_random % (vocabw_size - 1) + 1;
                                label = 0;
                            }
                            l2 = target * layer1_size;
                            f = 0;
                            for (c = 0; c < layer1_size; c++) f += neu1[c] * syn1neg[c + l2];
                            if (f > MAX_EXP) g = (label - 1) * alpha;
                            else if (f < -MAX_EXP) g = (label - 0) * alpha;
                            else g = (label - expTable[(int) ((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;
                            for (c = 0; c < layer1_size; c++) neu1e[c] += g * syn1neg[c + l2];
                            for (c = 0; c < layer1_size; c++) syn1neg[c + l2] += g * neu1[c];
                        }
                    // Learn weights input -> Projection
                    for (c = 0; c < layer1_size; c++) syn0[c + last_word * layer1_size] += neu1e[c];
                    for (i = 0; i < vocabw[last_word].gramlen; i++) {
                        for (c = 0; c < layer1_size; c++) syn0[c + vocabw[last_word].grams[i] * layer1_size + goff] += neu1e[c];
                    }
                }
        }
        sentence_position++;                                                    // Let's go for next word!!!
        if (sentence_position >= sentence_length) {                             // We are finally at the end of processing sentence
            sentence_length = 0;                                                // Let's go for a new one.
            continue;                                                           // Continue main loop
        }
    }
    fclose(fi);                                                                 // Clean up the mess.
    free(neu1);
    free(neu1e);
    pthread_exit(NULL);                                                         // Everything is done
}

/**
 Train the neural network. Run all threads!
 */
void TrainModel() {
    long a, b, c, d;                                                                      // Helpers
    FILE *fo;                                                                             // Output file
    pthread_t *pt = (pthread_t *)malloc(num_threads * sizeof(pthread_t));                 // Prepare threads
    printf("Starting training using file %s\n", train_file);                              // Starting process
    starting_alpha = alpha;                                                               // Initial alpha
    if (read_vocab_file[0] != 0) ReadVocab(); else LearnVocabFromTrainFile();             // Read vocabulary if possible
    if (save_vocab_file[0] != 0) SaveVocab();                                             // Save created vocabulary if needed
    if (output_file[0] == 0) return;                                                      // No output file
    InitNet();                                                                            // Init net
    if (negative > 0) InitUnigramTable();                                                 // Init unigram table if negative sampling
    start = clock();                                                                      // Start counting time
    for (a = 0; a < num_threads; a++) pthread_create(&pt[a], NULL, TrainModelThread, (void *)a);  // Create threads
    for (a = 0; a < num_threads; a++) pthread_join(pt[a], NULL);                                  // Wait for threads
    fo = fopen(output_file, "wb");                                                        // Print results
    if (classes == 0) {                                                                          // No classification classes
        // Save the word vectors
        fprintf(fo, "%lld %lld\n", vocabw_size, layer1_size);
        for (a = 0; a < vocabw_size; a++) {
            fprintf(fo, "%s ", vocabw[a].word);                                              // Save vocabulary
            if (binary) for (b = 0; b < layer1_size; b++) fwrite(&syn0[a * layer1_size + b], sizeof(real), 1, fo);  // Save binary (input -> Projection) weights
            else for (b = 0; b < layer1_size; b++) fprintf(fo, "%lf ", syn0[a * layer1_size + b]);                  // Save text (input -> Projection) weights
            fprintf(fo, "\n");
        }
    } else {
        // Run K-means on the word vectors
        int clcn = classes, iter = 10, closeid;
        int *centcn = (int *)malloc(classes * sizeof(int));                               // Classes counts
        int *cl = (int *)calloc(vocabw_size, sizeof(int));                               // Classes inclusion
        real closev, x;                                                                  // Helpers
        real *cent = (real *)calloc(classes * layer1_size, sizeof(real));                // Classes vectors
        for (a = 0; a < vocabw_size; a++) cl[a] = a % clcn;                              // Initial classes inclusion -> simple, just position % number of classes
        for (a = 0; a < iter; a++) {                                                     // Iterate over number of iteration
            for (b = 0; b < clcn * layer1_size; b++) cent[b] = 0;                        // Set classes vectors to 0
            for (b = 0; b < clcn; b++) centcn[b] = 1;                                    // Set classes count to 1
            for (c = 0; c < vocabw_size; c++) {                                          // Iterate over all words in vocab
                for (d = 0; d < layer1_size; d++) cent[layer1_size * cl[c] + d] += syn0[c * layer1_size + d];          // Add word vectors to corresponding classes vectors
                centcn[cl[c]]++;                                                         // Class distribution
            }
            for (b = 0; b < clcn; b++) {                                                 // Iterate over classes
                closev = 0;                                                              // Normalization helper
                for (c = 0; c < layer1_size; c++) {                                      // Iterate vectors
                    cent[layer1_size * b + c] /= centcn[b];                              // Make average values over all corresponding words
                    closev += cent[layer1_size * b + c] * cent[layer1_size * b + c];     // Summarize of second powers
                }
                closev = sqrt(closev);                                                   // sqrt(E a^2)
                for (c = 0; c < layer1_size; c++) cent[layer1_size * b + c] /= closev;   // Another normalization
            }
            for (c = 0; c < vocabw_size; c++) {                                          // Assign new classes
                closev = -10;                                                            // Maximum value for class
                closeid = 0;                                                             // Closest class number
                for (d = 0; d < clcn; d++) {                                             // Iterate over classes
                    x = 0;                                                               // Clear current value
                    for (b = 0; b < layer1_size; b++) x += cent[layer1_size * d + b] * syn0[c * layer1_size + b]; // Summarize class vector with word vector
                    if (x > closev) {                                                    // If more closer class
                        closev = x;                                                      // New maximum value
                        closeid = d;                                                     // Closest class number
                    }
                }
                cl[c] = closeid;                                                         // Assign best class for word
            }
        }
        // Save the K-means classes
        for (a = 0; a < vocabw_size; a++) fprintf(fo, "%s %d\n", vocabw[a].word, cl[a]);
        free(centcn);
        free(cent);
        free(cl);
    }
    fclose(fo);
}

/**
 Helper function for parsing arguments!
 */
int ArgPos(char *str, int argc, char **argv) {
    int a;
    for (a = 1; a < argc; a++)
        if (!strcmp(str, argv[a])) {
            if (a == argc - 1) {
                printf("Argument missing for %s\n", str);
                exit(1);
            }
            return a;
        }
    return -1;
}

/**
 Main function.
 */
int main(int argc, char **argv) {
    int i;
    if (argc == 1) {
        printf("Wordgram with mixed projection layer Vector estimation toolkit v1.0\n\n");
        printf("Options:\n");
        printf("Parameters for training:\n");
        printf("\t-train <file>\n");
        printf("\t\tUse text data from <file> to train the model\n");
        printf("\t-output <file>\n");
        printf("\t\tUse <file> to save the resulting word vectors / word clusters\n");
        printf("\t-size <int>\n");
        printf("\t\tSet size of word vectors; default is 100\n");
        printf("\t-window <int>\n");
        printf("\t\tSet max skip length between words; default is 5\n");
        printf("\t-sample <float>\n");
        printf("\t\tSet threshold for occurrence of words. Those that appear with higher frequency in the training data\n");
        printf("\t\twill be randomly down-sampled; default is 1e-3, useful range is (0, 1e-5)\n");
        printf("\t-hs <int>\n");
        printf("\t\tUse Hierarchical Softmax; default is 0 (not used)\n");
        printf("\t-negative <int>\n");
        printf("\t\tNumber of negative examples; default is 5, common values are 3 - 10 (0 = not used)\n");
        printf("\t-threads <int>\n");
        printf("\t\tUse <int> threads (default 1)\n");
        printf("\t-iter <int>\n");
        printf("\t\tRun more training iterations (default 5)\n");
        printf("\t-min-count <int>\n");
        printf("\t\tThis will discard words that appear less than <int> times; default is 5\n");
        printf("\t-alpha <float>\n");
        printf("\t\tSet the starting learning rate; default is 0.025 for skip-gram and 0.05 for CBOW\n");
        printf("\t-classes <int>\n");
        printf("\t\tOutput word classes rather than word vectors; default number of classes is 0 (vectors are written)\n");
        printf("\t-debug <int>\n");
        printf("\t\tSet the debug mode (default = 2 = more info during training)\n");
        printf("\t-binary <int>\n");
        printf("\t\tSave the resulting vectors in binary moded; default is 0 (off)\n");
        printf("\t-save-vocab <file>\n");
        printf("\t\tThe vocabulary will be saved to <file>\n");
        printf("\t-read-vocab <file>\n");
        printf("\t\tThe vocabulary will be read from <file>, not constructed from the training data\n");
        printf("\t-cbow <int>\n");
        printf("\t\tUse the continuous back of words model; default is 0 (skip-gram model)\n");
        printf("\t-ngrams <int>\n");
        printf("\t\tSize of n-grams; default is 4\n");
        printf("\t-unknown-gram <int>\n");
        printf("\t\tUnknown n-gram supported; default is 1 (0 = not used)\n");
        printf("\t-ngram-bounding <int>\n");
        printf("\t\tN-gram bounding supported; default is 1 (0 = not used)\n");
        printf("\nExamples:\n");
        printf("./word2vec -train data.txt -output vec.txt -size 200 -window 5 -sample 1e-4 -negative 5 -hs 0 -binary 0 -cbow 1 -iter 3 -ngram-bounding 1 -unknown-gram 0 -ngrams 4\n\n");
        return 0;
    }
    output_file[0] = 0;
    save_vocab_file[0] = 0;
    read_vocab_file[0] = 0;
    if ((i = ArgPos((char *)"-size", argc, argv)) > 0) layer1_size = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-train", argc, argv)) > 0) strcpy(train_file, argv[i + 1]);
    if ((i = ArgPos((char *)"-save-vocab", argc, argv)) > 0) strcpy(save_vocab_file, argv[i + 1]);
    if ((i = ArgPos((char *)"-read-vocab", argc, argv)) > 0) strcpy(read_vocab_file, argv[i + 1]);
    if ((i = ArgPos((char *)"-debug", argc, argv)) > 0) debug_mode = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-binary", argc, argv)) > 0) binary = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-cbow", argc, argv)) > 0) cbow = atoi(argv[i + 1]);
    if (cbow) alpha = 0.05;
    if ((i = ArgPos((char *)"-alpha", argc, argv)) > 0) alpha = atof(argv[i + 1]);
    if ((i = ArgPos((char *)"-output", argc, argv)) > 0) strcpy(output_file, argv[i + 1]);
    if ((i = ArgPos((char *)"-window", argc, argv)) > 0) window = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-sample", argc, argv)) > 0) sample = atof(argv[i + 1]);
    if ((i = ArgPos((char *)"-hs", argc, argv)) > 0) hs = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-negative", argc, argv)) > 0) negative = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-threads", argc, argv)) > 0) num_threads = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-iter", argc, argv)) > 0) iter = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-min-count", argc, argv)) > 0) min_count = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-classes", argc, argv)) > 0) classes = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-ngrams", argc, argv)) > 0) ngrams = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-unknown-gram", argc, argv)) > 0) unknown = atoi(argv[i + 1]);
    if ((i = ArgPos((char *)"-ngram-bounding", argc, argv)) > 0) boundaries = atoi(argv[i + 1]);
    vocabg = (struct vocab_gram *)calloc(vocabg_max_size, sizeof(struct vocab_gram));               // Allocate initial vocab!
    vocabw = (struct vocab_word *)calloc(vocabw_max_size, sizeof(struct vocab_word));               // Allocate initial list of words
    vocabg_hash = (int *)calloc(vocab_hash_size, sizeof(int));                                      // Allocate hash table
    vocabw_hash = (int *)calloc(vocab_hash_size, sizeof(int));                                      // Allocate hash table
    expTable = (real *)malloc((EXP_TABLE_SIZE + 1) * sizeof(real));                                 // Allocate exponencial table
    for (i = 0; i < EXP_TABLE_SIZE; i++) {
        expTable[i] = exp((i / (real)EXP_TABLE_SIZE * 2 - 1) * MAX_EXP);                            // Pre-compute the exp() table
        expTable[i] = expTable[i] / (expTable[i] + 1);                                              // Pre-compute f(x) = x / (x + 1) -> range(0, 1)
    }
    TrainModel();
    return 0;                                                                                       // Everything done without errors.
}
