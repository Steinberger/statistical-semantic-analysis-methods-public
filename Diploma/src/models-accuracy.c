//  Copyright 2013 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

// Questions should be lowercased, especially if using non-ascii character!!! Using tolower function!

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

const long long max_size = 2000;         // max length of strings and vectors!!! When projection divided by 2!!!
const long long N = 1;                   // number of closest words
const long long max_w = 50;              // max length of vocabulary entries

const int vocab_hash_size = 30000000;    // Maximum 30 * 0.7 = 21M words in the vocabulary

char *vocab2;                            // Second vocab
int *vocab2_hash;                        // Hash table for second vocab

// Returns hash value of a word
int GetWordHash(char *word) {
    unsigned long long a, hash = 0;
    for (a = 0; a < strlen(word); a++) hash = hash * 257 + word[a];
    hash = hash % vocab_hash_size;
    return hash;
}

// Returns position of a word in the vocabulary; if the word is not found, returns -1
int SearchVocab(char *word) {
    unsigned int hash = GetWordHash(word);
    while (1) {
        if (vocab2_hash[hash] == -1) return -1;
        if (!strcmp(word, &vocab2[vocab2_hash[hash] * max_w])) return vocab2_hash[hash];
        hash = (hash + 1) % vocab_hash_size;
    }
}

// Hash second vocabulary for fastest evaluation
void hashVocab(int size) {
    int i, hash;
    for (i = 0; i < vocab_hash_size; i++) vocab2_hash[i] = -1;
    for(i = 0; i < size; i++) {
        hash = GetWordHash(&vocab2[i * max_w]);
        while(vocab2_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
        vocab2_hash[hash] = i;
    }
}

// Main evaluation function
int main(int argc, char **argv)
{
    FILE *f;
    char st1[max_size], st2[max_size], st3[max_size], st4[max_size], bestw[N][max_size], file_name[max_size], file2_name[max_size];
    float dist, len, bestd[N], vec[max_size];
    long long words, size, a, b, c, d, b1, b2, b3, c1, c2, c3, z, threshold = 0, boundary = 5, concat = 1;
    float *M, *M2;
    char *vocab;
    int TCN, CCN = 0, TACN = 0, CACN = 0, SECN = 0, SYCN = 0, SEAC = 0, SYAC = 0, QID = 0, TQ = 0, TQS = 0, QCN = 0;
    if (argc < 3) {
        printf("Input questions should be lowercased, especially when using non-ascii characters (tolower function is used)\n");
        printf("Usage: ./compute-accuracy <MODEL1> <MODEL2> <TYPE> <SIM_CAT> <threshold>\nwhere \n");
        printf("\tMODEL files contains word projections\n");
        printf("\tTYPE type of joining the models (1 - concat:default, 0 - summary)");
        printf("\tSIM_CAT number of similarity categories (default = 5)\n");
        printf("\tthreshold is used to reduce vocabulary of the model for fast approximate evaluation (0 = off, otherwise typical value is 30000)\n");
        return 0;
    }
    strcpy(file_name, argv[1]);
    strcpy(file2_name, argv[2]);
    if (argc > 3) concat = atoi(argv[3]);
    if (argc > 4) boundary = atoi(argv[4]);                       // Boundary between semantics and syntactics questions -> number of semantics
    if (argc > 5) threshold = atoi(argv[5]);                      // Reduce vocabulary to faster evaluation

    printf("Loading models...\n");

    // Load first model
    printf("Loading first model...\n");
    f = fopen(file_name, "rb");
    if (f == NULL) {
        printf("Input file not found\n");
        return -1;
    }
    fscanf(f, "%lld", &words);
    if (threshold) if (words > threshold) words = threshold;
    fscanf(f, "%lld", &size);
    vocab = (char *)malloc(words * max_w * sizeof(char));
    M = (float *)malloc(words * size * sizeof(float));
    if (M == NULL) {
        printf("Cannot allocate memory: %lld MB\n", words * size * sizeof(float) / 1048576);
        return -1;
    }
    for (b = 0; b < words; b++) {
        a = 0;
        while (1) {
            vocab[b * max_w + a] = fgetc(f);
            if (feof(f) || (vocab[b * max_w + a] == ' ')) break;
            if ((a < max_w) && (vocab[b * max_w + a] != '\n')) a++;
        }
        vocab[b * max_w + a] = 0;
        for (a = 0; a < max_w; a++) vocab[b * max_w + a] = tolower(vocab[b * max_w + a]);
        for (a = 0; a < size; a++) fread(&M[a + b * size], sizeof(float), 1, f);
        len = 0;
        for (a = 0; a < size; a++) len += M[a + b * size] * M[a + b * size];
        len = sqrt(len);
        for (a = 0; a < size; a++) M[a + b * size] /= len;
    }
    fclose(f);

    // Load second model
    printf("Loading second model...\n");
    vocab2_hash = (int *)calloc(vocab_hash_size, sizeof(int));
    f = fopen(file2_name, "rb");
    if (f == NULL) {
        printf("Input file not found\n");
        return -1;
    }
    fscanf(f, "%lld", &words);
    if (threshold) if (words > threshold) words = threshold;
    fscanf(f, "%lld", &size);
    vocab2 = (char *)malloc(words * max_w * sizeof(char));
    M2 = (float *)malloc(words * size * sizeof(float));
    if (M2 == NULL) {
        printf("Cannot allocate memory: %lld MB\n", words * size * sizeof(float) / 1048576);
        return -1;
    }
    for (b = 0; b < words; b++) {
        a = 0;
        while (1) {
            vocab2[b * max_w + a] = fgetc(f);
            if (feof(f) || (vocab2[b * max_w + a] == ' ')) break;
            if ((a < max_w) && (vocab2[b * max_w + a] != '\n')) a++;
        }
        vocab2[b * max_w + a] = 0;
        for (a = 0; a < max_w; a++) vocab2[b * max_w + a] = tolower(vocab2[b * max_w + a]);
        for (a = 0; a < size; a++) fread(&M2[a + b * size], sizeof(float), 1, f);
        len = 0;
        for (a = 0; a < size; a++) len += M2[a + b * size] * M2[a + b * size];
        len = sqrt(len);
        for (a = 0; a < size; a++) M2[a + b * size] /= len;
    }
    hashVocab(words);                                               // Hash second vocab for faster evaluation
    fclose(f);

    printf("Models loaded\n");

    TCN = 0;
    while (1) {
        for (a = 0; a < N; a++) bestd[a] = 0;
        for (a = 0; a < N; a++) bestw[a][0] = 0;
        scanf("%s", st1);
        for (a = 0; a < strlen(st1); a++) st1[a] = tolower(st1[a]);
        if ((!strcmp(st1, ":")) || (!strcmp(st1, "EXIT")) || feof(stdin)) {
            if (TCN == 0) TCN = 1;
            if (QID != 0) {
                printf("ACCURACY TOP1: %.2f %%  (%d / %d / %d)\n", CCN / (float)TCN * 100, CCN, TCN, QCN);
                printf("Total accuracy: %.2f %%   Semantic accuracy: %.2f %%   Syntactic accuracy: %.2f %% \n", CACN / (float)TACN * 100, SEAC / (float)SECN * 100, SYAC / (float)SYCN * 100);
            }
            QID++;
            scanf("%s", st1);
            if (feof(stdin)) break;
            printf("%s:\n", st1);
            TCN = 0;
            CCN = 0;
            QCN = 0;
            continue;
        }
        if (!strcmp(st1, "EXIT")) break;
        TQ++;
        QCN++;
        scanf("%s", st2);
        for (a = 0; a < strlen(st2); a++) st2[a] = tolower(st2[a]);
        scanf("%s", st3);
        for (a = 0; a<strlen(st3); a++) st3[a] = tolower(st3[a]);
        scanf("%s", st4);
        for (a = 0; a < strlen(st4); a++) st4[a] = tolower(st4[a]);
        for (b = 0; b < words; b++) if (!strcmp(&vocab[b * max_w], st1)) break;
        b1 = b;
        for (b = 0; b < words; b++) if (!strcmp(&vocab[b * max_w], st2)) break;
        b2 = b;
        for (b = 0; b < words; b++) if (!strcmp(&vocab[b * max_w], st3)) break;
        b3 = b;
        if (b1 == words) continue;                                                                  // One of first three words is not in first model
        if (b2 == words) continue;
        if (b3 == words) continue;
        if(strcmp(&vocab[b1 * max_w], &vocab2[b1 * max_w])) {                                       // Vocabs items do not match
            c1 = SearchVocab(&vocab[b1 * max_w]);                                                   // Find equivalent in second vocab using hash
        } else c1 = b1;                                                                             // Vocabs items match
        if(strcmp(&vocab[b2 * max_w], &vocab2[b2 * max_w])) {
            c2 = SearchVocab(&vocab[b2 * max_w]);
        } else c2 = b2;
        if(strcmp(&vocab[b3 * max_w], &vocab2[b3 * max_w])) {
            c3 = SearchVocab(&vocab[b3 * max_w]);
        } else c3 = b3;
        if (c1 == -1) continue;                                                                     // One of first three words is not in second model
        if (c2 == -1) continue;
        if (c3 == -1) continue;
        for (a = 0; a < N; a++) bestd[a] = 0;
        for (a = 0; a < N; a++) bestw[a][0] = 0;
        for (b = 0; b < words; b++) if (!strcmp(&vocab[b * max_w], st4)) break;
        if (b == words) continue;                                                                   // Fourth word is not in first model
        if(strcmp(&vocab[b * max_w], &vocab2[b * max_w])) {
            if(SearchVocab(st4) == -1) continue;                                                    // Fourth word is not in second model
        }
        if(concat) {                                                                                // Concatenation of vectors
            for (a = 0; a < size; a++) vec[a] = (M[a + b2 * size] - M[a + b1 * size]) + M[a + b3 * size];
            for (a = 0; a < size; a++) vec[a + size] = (M2[a + c2 * size] - M2[a + c1 * size]) + M2[a + c3 * size];
        } else {                                                                                    // Summation of vectors
            for (a = 0; a < size; a++) vec[a] = ((M[a + b2 * size] + M2[a + c2 * size]) - (M[a + b1 * size] + M2[a + c1 * size])) + (M[a + b3 * size] + M2[a + c3 * size]);
        }
        TQS++;
        for (c = 0; c < words; c++) {
            if (c == b1) continue;
            if (c == b2) continue;
            if (c == b3) continue;
            if (strcmp(&vocab[c * max_w], &vocab2[c * max_w])) {
                z = SearchVocab(&vocab[c * max_w]);
            } else z = c;
            if (z == -1) continue;
            dist = 0;
            if(concat) {                                                                            // Concatenation of vectors
                for (a = 0; a < size; a++) dist += vec[a] * M[a + c * size];
                for (a = 0; a < size; a++) dist += vec[a + size] * M2[a + z * size];
            } else {                                                                                // Summation of vectors
                for (a = 0; a < size; a++) dist += vec[a] * (M[a + c * size] + M2[a + z * size]);
            }
            for (a = 0; a < N; a++) {
                if (dist > bestd[a]) {
                    for (d = N - 1; d > a; d--) {
                        bestd[d] = bestd[d - 1];
                        strcpy(bestw[d], bestw[d - 1]);
                    }
                    bestd[a] = dist;
                    strcpy(bestw[a], &vocab[c * max_w]);
                    break;
                }
            }
        }
        if (!strcmp(st4, bestw[0])) {
            CCN++;
            CACN++;
            if (QID <= boundary) SEAC++; else SYAC++;
        }
        if (QID <= boundary) SECN++; else SYCN++;
        TCN++;
        TACN++;
    }
    printf("Questions seen / total: %d %d   %.2f %% \n", TQS, TQ, TQS/(float)TQ*100);
    return 0;
}