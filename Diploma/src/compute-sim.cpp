//  Copyright 2013 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

// Pairs should be lowercased, especially if using non-ascii character!!! Using tolower function!

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "lib/statistics.h"

const long long max_size = 2000;         // max length of strings
const long long N = 1;                   // number of closest words
const long long max_w = 50;              // max length of vocabulary entries

int main(int argc, char **argv) {
    FILE *f;
    char st1[max_size], st2[max_size], file_name[max_size];
    float dist, len;
    double  ar1[max_size], ar2[max_size];                       // Temp arrays for results
    alglib::real_1d_array nar1, nar2;                           // ALGLIB 1-D vectors
    alglib::ae_int_t seen = 0, all = 0;                         // Seen pairs, all pairs
    long long words, size, a, b, b1, b2;
    int score;                                                  // Temp variable for score
    float *M;                                                   // Word vectors
    char *vocab;                                                // Vocabulary
    if (argc < 2) {
        printf("Input pairs should be lowercased, especially when using non-ascii characters (tolower function is used)\n");
        printf("Usage: ./compute-sim <FILE>\nwhere FILE contains word projections\n");
        return 0;
    }

    // Load model and vocabulary
    printf("Loading model...\n");
    strcpy(file_name, argv[1]);
    f = fopen(file_name, "rb");
    if (f == NULL) {
        printf("Input file not found\n");
        return -1;
    }
    fscanf(f, "%lld", &words);
    fscanf(f, "%lld", &size);
    vocab = (char *)malloc(words * max_w * sizeof(char));
    M = (float *)malloc(words * size * sizeof(float));
    if (M == NULL) {
        printf("Cannot allocate memory: %lld MB\n", words * size * sizeof(float) / 1048576);
        return -1;
    }
    for (b = 0; b < words; b++) {
        a = 0;
        while (1) {
            vocab[b * max_w + a] = fgetc(f);
            if (feof(f) || (vocab[b * max_w + a] == ' ')) break;
            if ((a < max_w) && (vocab[b * max_w + a] != '\n')) a++;
        }
        vocab[b * max_w + a] = 0;
        for (a = 0; a < max_w; a++) vocab[b * max_w + a] = tolower(vocab[b * max_w + a]);
        for (a = 0; a < size; a++) fread(&M[a + b * size], sizeof(float), 1, f);
        len = 0;
        for (a = 0; a < size; a++) len += M[a + b * size] * M[a + b * size];
        len = sqrt(len);
        for (a = 0; a < size; a++) M[a + b * size] /= len;
    }
    fclose(f);
    printf("Model loaded...\n");

    // Evaluate dataset
    while(1) {
        scanf("%s", st1);
        for (a = 0; a < strlen(st1); a++) st1[a] = tolower(st1[a]);
        if(feof(stdin)) break;
        all++;
        scanf("%s", st2);
        for (a = 0; a < strlen(st2); a++) st2[a] = tolower(st2[a]);
        scanf("%d", &score);
        for (b = 0; b < words; b++) if (!strcmp(&vocab[b * max_w], st1)) break;
        b1 = b;
        for (b = 0; b < words; b++) if (!strcmp(&vocab[b * max_w], st2)) break;
        b2 = b;
        if (b1 == words)continue;
        if (b2 == words) continue;
        if (b == words) continue;

        dist = 0;
        for (a = 0; a < size; a++) dist += M[a + b1 * size] * M[a + b2 * size];
        ar1[seen] = score;
        ar2[seen] = dist;
        seen++;
    }
    nar1.setcontent(seen, ar1);     // Prepare ALGLIB vectors
    nar2.setcontent(seen, ar2);

    // Print results
    printf("\nResults:\n");
    printf("--------------------------------------------------------------------\n");
    printf("Pairs seen: %ld of %ld\n\n", seen, all);
    printf("Pearson correlation: %f\n", alglib::pearsoncorr2(nar1, nar2, seen));
    printf("Spearman correlation: %f\n", alglib::spearmancorr2(nar1, nar2, seen));
    printf("--------------------------------------------------------------------\n");
    return 0;
}
