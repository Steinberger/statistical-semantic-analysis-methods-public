#!/usr/bin/env bash
DATA_DIR=../data
BIN_DIR=../bin
SRC_DIR=../src

TEXT_DATA=$DATA_DIR/news.2012.en.shuffled
PHRASES_DATA=$DATA_DIR/news.2012.en.shuffled-norm1-phrase1
PHRASES_VECTOR_DATA=$DATA_DIR/vectors-phrase.bin

pushd ${SRC_DIR} && make; popd

if [ ! -e $PHRASES_VECTOR_DATA ]; then
  
  if [ ! -e $PHRASES_DATA ]; then
    
    if [ ! -e $TEXT_DATA ]; then
      wget http://www.statmt.org/wmt14/training-monolingual-news-crawl/news.2012.en.shuffled.gz
      gzip -d news.2012.en.shuffled.gz -f
    fi
    echo -----------------------------------------------------------------------------------------------------
    echo -- Creating phrases...
    sed -e "s/’/'/g" -e "s/′/'/g" -e "s/''/ /g" < $TEXT_DATA | tr -c "A-Za-z'_ \n" " " > $TEXT_DATA-norm0
    time ./word2phrase -train $TEXT_DATA-norm0 -output $TEXT_DATA-norm0-phrase0 -threshold 200 -debug 2
    time ./word2phrase -train $TEXT_DATA-norm0-phrase0 -output $TEXT_DATA-norm0-phrase1 -threshold 100 -debug 2
    tr A-Z a-z < $TEXT_DATA-norm0-phrase1 > $PHRASES_DATA
    
  fi

  echo -----------------------------------------------------------------------------------------------------
  echo -- Training vectors from phrases...
  time $BIN_DIR/word2vec -train $PHRASES_DATA -output $PHRASES_VECTOR_DATA -cbow 1 -size 200 -window 10 -negative 25 -hs 0 -sample 1e-5 -threads 12 -binary 1 -iter 15
  
fi

echo -----------------------------------------------------------------------------------------------------
echo -- distance...

$BIN_DIR/distance $PHRASES_VECTOR_DATA
