#!/bin/bash
#PBS -N moses-CLUST-compar-0-8
#PBS -q q_1w
#PBS -l nodes=1:ppn=16:nfs4
#PBS -l scratch=1000000
#PBS -l mem=40gb
#PBS -j oe
#PBS -m e

# Inicializace systemu modulu:
. /packages/run/modules-2.0/init/sh

# inicializace konkretniho programu
module add moses-2.1.1

#nastaveni uklidu pri chybe nebo ukonceni
trap "mv $SCRATCHDIR /storage/brno2/home/konopik/recovery/" TERM EXIT

# Nastaveni promennych
factors="0 8"

cp /storage/plzen1/home/konopik/run-moses-clusters-new.sh $SCRATCHDIR

cd $SCRATCHDIR
mkdir experiment
mkdir corpus
mkdir lm
mkdir evaluation

#Warning: NO STEM 25 -> 20 GB
#Warning: Time efficient Nbest -> 40 GB
#Warning: NO STEM LM
#Warning: PROC 16
#Warning: NO all copy
#cp /storage/plzen1/home/konopik/corpus/czeng-1.0/00*.?? ./corpus/

cat /storage/plzen1/home/konopik/corpus/czeng-1.0/0?train.combined.en > ./corpus/0xtrain.active.en
cat /storage/plzen1/home/konopik/corpus/czeng-1.0/0?train.combined.cs > ./corpus/0xtrain.combined.cs
extract-factors.pl ./corpus/0xtrain.combined.cs $factors > ./corpus/0xtrain.active.cs

head -n 10000 /storage/plzen1/home/konopik/corpus/czeng-1.0/test/96train.combined.cs > ./corpus/source.all
extract-factors.pl ./corpus/source.all $factors > ./corpus/source; rm ./corpus/source.all;
head -n 10000 /storage/plzen1/home/konopik/corpus/czeng-1.0/test/96train.surf.en > evaluation/ref

head -n 10000 /storage/plzen1/home/konopik/corpus/czeng-1.0/heldout/94train.combined.cs > ./corpus/heldout.src.all
extract-factors.pl ./corpus/heldout.src.all $factors > ./corpus/heldout.src; rm ./corpus/heldout.src.all;
head -n 10000 /storage/plzen1/home/konopik/corpus/czeng-1.0/heldout/94train.surf.en > ./corpus/heldout.ref

cp /storage/plzen1/home/konopik/lm/czeng/lm.surf.blm.en ./lm/
#cp /storage/plzen1/home/konopik/lm/czeng/lm.stem.blm.en ./lm/

clean-corpus-n.perl ./corpus/0xtrain.active cs en ./corpus/0xtrain.clean 1 80

train-model.perl \
    --external-bin-dir /software/moses-2.1.1/bin \
    --cores 16 \
    --corpus $SCRATCHDIR/corpus/0xtrain.clean \
    --root-dir experiment \
    --f cs --e en \
    --lm 0:5:$SCRATCHDIR/lm/lm.surf.blm.en:8 \
    --alignment-factors 0-0 \
    --reordering msd-bidirectional-fe \
    --reordering-factors 0-0+1-0 \
    --translation-factors 0-0+1-0  \
    --decoding-steps t0:t1

#    --alignment-factors 1-1 \
#    --reordering-factors 0-0+1-0+2-0 \
#    --translation-factors 0-0+1-0+2-0  \
#    --decoding-steps t0:t1:t2

#train-model.perl \
#    --external-bin-dir  /software/moses-2.1.1/bin \
#    --cores 16 \
#    --corpus $SCRATCHDIR/corpus/00train.combined.clean \
#    --root-dir experiment \
#    --f cs --e en \
#    --lm 0:5:$SCRATCHDIR/lm/lm.surf.blm.en:8 \
#    --lm 1:5:$SCRATCHDIR/lm/lm.stem.blm.en:8 \
#    --alignment-factors 1-1 \
#    --reordering msd-bidirectional-fe \
#    --reordering-factors 0-0,1+1-0,1+5-0,1+6-0,1+7-0,1+8-0,1+9-0,1+10-0,1 \
#    --translation-factors 0-0,1+1-0,1+5-0,1+6-0,1+7-0,1+8-0,1+9-0,1+10-0,1  \
#    --decoding-steps t0:t1:t2:t4:t5:t6:t7

#    --lm 2:5:$SCRATCHDIR/lm/lm.tag.blm.en:8 \
#    --reordering-factors 1-1+2-2+0-0,2 \
#    --translation-factors 1-1+2-2+0-0,2 \
#    --generation-factors 1-2+1,2-0 \


mkdir tmp

mert-moses.pl $SCRATCHDIR/corpus/heldout.src $SCRATCHDIR/corpus/heldout.ref \
  /software/moses-2.1.1/bin/moses  $SCRATCHDIR/experiment/model/moses.ini \
  --working-dir $SCRATCHDIR/tmp \
  --decoder-flags "-threads 16 -v 0" \
  --maximum-iterations 10 \
  --rootdir /software/moses-2.1.1/scripts -mertdir /software/moses-2.1.1/bin #\
#  --efficient_scorenbest_flag \
#  --no-filter-phrase-table

#  --decoder-flags "-threads 16 -v 0 -distinct-nbest" \

#train-model.perl \
#    --external-bin-dir /software/moses-2.1.1/bin \
#    --cores 4 \
#    --corpus $SCRATCHDIR/corpus/00train.lc \
#    --root-dir experiment \
#    --f cs --e en \
#    --lm 0:5:$SCRATCHDIR/lm/lm.surf.blm.en:8 \
#    --lm 2:5:$SCRATCHDIR/lm/lm.tag.blm.en:8 \
#    --reordering msd-bidirectional-fe \
#    --reordering-factors 0-0,2+1-0,2 \
#    --translation-factors 0-0,2+1-0,2 \
#    --decoding-steps t0:t1


#train-model.perl \
#    --external-bin-dir /software/moses-2.1.1/bin \
#    --cores 4 \
#    --corpus $SCRATCHDIR/corpus/00train.lc \
#    --root-dir experiment \
#    --f cs --e en \
#    --lm 0:5:$SCRATCHDIR/lm/lm.surf.blm.en:8 \
#    --reordering msd-bidirectional-fe

filter-model-given-input.pl $SCRATCHDIR/evaluation/filtered \
     $SCRATCHDIR/tmp/moses.ini \
     $SCRATCHDIR/corpus/source \
     -Binarizer "/software/moses-2.1.1/bin/processPhraseTable"

moses -f $SCRATCHDIR/evaluation/filtered/moses.ini \
    -threads 16 \
    < $SCRATCHDIR/corpus/source \
    > $SCRATCHDIR/evaluation/translation.en \ 
    2> $SCRATCHDIR/evaluation/translation.log

multi-bleu.perl -lc \
	$SCRATCHDIR/evaluation/ref \
	< $SCRATCHDIR/evaluation/translation.en \
	> $SCRATCHDIR/evaluation/bleu.out


#    -threads 4 \
#moses -f $SCRATCHDIR/experiment/model/moses.ini \
#    < $SCRATCHDIR/factored-corpus/proj-syndicate.1000.de \
#    > $SCRATCHDIR/evaluation/proj-syndicate.1000.en \ 
#    2> $SCRATCHDIR/evaluation/proj-syndicate.out

#multi-bleu.perl \
#    -lc $SCRATCHDIR/factored-corpus/proj-syndicate.1000.0-0.en \
#0    < $SCRATCHDIR/evaluation/proj-syndicate.1000.en

pwd
ls -alh

mkdir /storage/plzen1/home/konopik/experiment_$PBS_JOBID
mv $SCRATCHDIR /storage/plzen1/home/konopik/experiment_$PBS_JOBID
