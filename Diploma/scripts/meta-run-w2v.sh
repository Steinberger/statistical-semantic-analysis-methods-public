#!/bin/bash
#PBS -N w2v-classic-text8-3
#PBS -q q_2h
#PBS -l nodes=1:ppn=4:nfs4
#PBS -l scratch=1gb
#PBS -l mem=2g
#PBS -j oe
#PBS -m e

# User home
USER_HOME=/storage/plzen1/home/fenic
CORPUS_FILE=text8
MODEL_FILE=${CORPUS_FILE}_dim200_neg5_sg_w5_i
DATASET_FILE=czech_emb_corpus_words_lower.txt

# Initialization of additional modules
. /packages/run/modules-2.0/init/sh

# Module configuration

# In case of failure copy data to different location
trap "mv $SCRATCHDIR /storage/brno2/home/fenic/recovery/" TERM EXIT

# Copy script to scratch directory
cp $USER_HOME/meta-run-w2v.sh $SCRATCHDIR

cd $SCRATCHDIR
mkdir corpus
mkdir data
mkdir bin
mkdir dataset
mkdir results

# Copy corpus file
cp $USER_HOME/myword2vec/corpus/$CORPUS_FILE ./corpus
cp $USER_HOME/myword2vec/bin/word2vec ./bin
cp $USER_HOME/myword2vec/bin/compute-accuracy ./bin
cp $USER_HOME/myword2vec/dataset/$DATASET_FILE ./dataset

# Run w2v
./bin/word2vec -train ./corpus/${CORPUS_FILE} -output ./data/${MODEL_FILE} -size 200 -binary 1 -cbow 0 -window 5 -sample 1e-3 -hs 0 -negative 5 -threads 4 -ngrams 4 -unknown-gram 1 -ngram-bounding 1 -init-vectors 0 -iter 1 -debug 0

# Evaluate model
./bin/compute-accuracy ./data/${MODEL_FILE} < ./dataset/${DATASET_FILE} > ./results/${MODEL_FILE}

pwd
ls -alh

# Copy scratch dir to home dir
mv $SCRATCHDIR $USER_HOME/experiments
